import sys
sys.path.append("simulator/src")
import simulator.resource_allocator as resource_allocator
from conftest import env, network, sfc

class TestResourceAllocator:


    # Environemnt parameters are handed over before SFC processing
    def test_prepare_sfc_parameters(self, sfc, network, env):   
        ra = resource_allocator.ResourceAllocator(env, network)

        sfc = ra._add_env_parameters(sfc)

        for function in sfc.graph.nodes.values():
            assert "env" in function["parameters"].keys()
            assert function["parameters"]["env"] == env

    
    # A network status update is correctly performed
    def test_network_status_update(self, network, env):
        ra = resource_allocator.ResourceAllocator(env, network)
        ra.update_network_status()

        assert ra.network_status
        assert list(ra.network_status.nodes) == list(ra.mec_nodes)
        assert list(ra.network_status.edges) == list(ra.mec_links)

        for network_id in ra.mec_nodes:
            thing = network.nodes[network_id]
            for resource_id, resource in thing["resources"].items():
                assert ra.network_status.nodes[network_id][resource_id].items() <= resource.__dict__.items()

        for network_id in ra.mec_links:
            thing = network.edges[network_id]
            for resource_id, resource in thing["resources"].items():
                assert ra.network_status.edges[network_id][resource_id].items() <= resource.__dict__.items()

