import sys
sys.path.append("simulator/src")
from schema import Schema, And
import networkx as nx
import pytest
import simpy
from simulator.resources import NodeResource, LinkResource
from simulator.network import NetworkParser
from env_test import NETWORK_TOPO_FILE

class TestNetworkParser:

    # Read JSON from file to graph
    def test_read_json_from_file(self):
        filename = NETWORK_TOPO_FILE
        env = simpy.Environment()
        np = NetworkParser(env)
        graph = np._read_network_from_file(filename)
        assert isinstance(graph, nx.DiGraph)
    
    # Raise error if parameter is not string
    def test_raise_error_if_not_string(self):
        filename = 12
        env = simpy.Environment()
        np = NetworkParser(env)
        with pytest.raises(TypeError) as exception:
            graph = np._read_network_from_file(filename)
    
    # Raiser error with wrong network file format
    def test_raise_error_if_not_jsongml(self):
        filename = "network_topology.txt"
        env = simpy.Environment()
        np = NetworkParser(env)
        with pytest.raises(NotImplementedError) as exception:
            graph = np._read_network_from_file(filename)

    # Build network works
    def test_build_network_from_file(self):
        """Test all necessary node attributes are initialized correctly
        """
        node_attribute_scheme = Schema({
            "type": str, 
            "position": And(list, lambda t: len(t) == 2), 
            "resources": {str: NodeResource},
            "resource_specs": {str: dict}
        })
        link_attribute_scheme = Schema({
            "type": str,
            "resources": {str: LinkResource},
            "resource_specs": {str: dict}
        })
        filename = NETWORK_TOPO_FILE
        env = simpy.Environment()
        np = NetworkParser(env)
        graph = np.build_network_from_file(filename)
        for node_data in graph.nodes.values():
            assert node_attribute_scheme.validate(node_data)
        for link_data in graph.edges.values():
            assert link_attribute_scheme.validate(link_data)

    
    

