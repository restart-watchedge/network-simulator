import sys
sys.path.append("simulator/src")
import pytest
from unittest.mock import MagicMock, patch
import networkx as nx
import json
import copy
import simulator.event_dispatcher as event_dispatcher
import simulator.algorithm as algorithm
from simulator.events import BaseEvent
from simulator.sfc import SFC
from conftest import env, network, sfc, algorithm, link_param
class TestEventDispatcher:

    def place_sfc(self, sfc):
            node_list = ["edge_1_1", "edge_2_1", "cloud_3_1"]
            i=0
            for function in sfc.graph.nodes.values():
                    function["parameters"].update({
                        "network_id": node_list[i % len(node_list)],
                        "resource_id": "computational_resource"
                    })
                    i+=1
            return sfc

    # Assert placement of SFC provides correct output
    @patch("simulator.event_dispatcher.ResourceAllocator")
    def test_request_action(self, mock_ra, env, sfc, algorithm):
        evd = event_dispatcher.EventDispatcher(env, algorithm)
        mock_ra().get_allocation.return_value = sfc

        placed_sfc = evd.request_action(sfc)

        assert isinstance(placed_sfc, SFC)
        assert isinstance(placed_sfc.graph, nx.DiGraph)
        assert placed_sfc == sfc

    # Assert SFC link is realized as chain of link events
    def test_create_link(self, env, network, sfc, algorithm, link_param):
        evd = event_dispatcher.EventDispatcher(env, algorithm)
        node_list = list(network.nodes)
        node_1 = node_list[0]
        node_2 = node_list[1]
        sfc.graph.add_edge(node_1, node_2, parameters=link_param)
        sfc_link_list = list(sfc.graph.edges)
        sfc_link = sfc_link_list[0]
        path = nx.shortest_path(network, node_1, node_2)
        link_event_list = evd.create_link(path, sfc=sfc, sfc_link=sfc_link)
        for event in link_event_list:
            assert isinstance(event, BaseEvent)
            for parameter_id, parameter in sfc.graph.edges[sfc_link]["parameters"].items():
                if parameter_id != "request_timeout":
                    assert event.__dict__[parameter_id] == parameter

    # Assert SFC node placement on same network node results in no link event
    def test_create_link_w_empty_path(self, env, network, sfc, algorithm, link_param):
        evd = event_dispatcher.EventDispatcher(env, algorithm)
        node_list = list(network.nodes)
        node_1 = node_list[0]
        node_2 = node_list[0]
        sfc.graph.add_edge(node_1, node_2, parameters=link_param)
        sfc_link_list = list(sfc.graph.edges)
        sfc_link = sfc_link_list[0]
        path = nx.shortest_path(network, node_1, node_2)
        link_event_list = evd.create_link(path, sfc=sfc, sfc_link=sfc_link)
        assert not link_event_list

    # Assert node event creation respects API (object type and parameter passing)
    def test_create_node_event(self, env, network, sfc, algorithm):
        evd = event_dispatcher.EventDispatcher(env, algorithm)
        sample_sfc_node = list(sfc.graph.nodes)[0]
        sfc.graph.nodes[sample_sfc_node]["parameters"].update({
                        "network_id": list(network.nodes)[0],
                        "resource_id": "computational_resource"
                    })
        
        node_event = evd.create_node_event(sfc, sample_sfc_node)

        assert isinstance(node_event, BaseEvent)
        for parameter_id, parameter in sfc.graph.nodes[sample_sfc_node]["parameters"].items():
            assert node_event.__dict__[parameter_id] == parameter

    # Assert deployment of SFC results in a schedule graph and empty SFC graph
    @patch("simulator.event_dispatcher.ResourceAllocator")
    def test_deploy_sfc_has_correct_api(self, mock_ra, env, sfc, algorithm):
        evd = event_dispatcher.EventDispatcher(env, algorithm)
        mock_ra().get_allocation.return_value = self.place_sfc(sfc)

        placed_sfc = evd.request_action(sfc)

        env.process(evd.deploy_sfc(placed_sfc))
        env.run()

        assert isinstance(placed_sfc.schedule, nx.DiGraph)
        assert not placed_sfc.graph.nodes

    
    # Assert correct deployment of schedule graph
    @patch("simulator.event_dispatcher.ResourceAllocator")
    def test_deploy_sfc_has_correct_schedule(self, mock_ra, env, sfc, algorithm):
        evd = event_dispatcher.EventDispatcher(env, algorithm)
        mock_ra().get_allocation.return_value = self.place_sfc(sfc)

        placed_sfc = evd.request_action(sfc)

        env.process(evd.deploy_sfc(placed_sfc))
        env.run()

        assert nx.is_directed_acyclic_graph(placed_sfc.schedule)
        for node in sfc.schedule.nodes.values():
            assert node["event_obj"].processed
    
    # Assert SFC deployment lets simulation time pass
    @patch("simulator.event_dispatcher.ResourceAllocator")
    def test_deploy_sfc_function(self, mock_ra, env, sfc, algorithm):
        evd = event_dispatcher.EventDispatcher(env, algorithm)
        mock_ra().get_allocation.return_value = self.place_sfc(sfc)

        placed_sfc = evd.request_action(sfc)

        env.run(until=env.process(evd.deploy_sfc(placed_sfc)))

        assert evd.env.collector.sfc_logs
        assert evd.env.collector.sfc_logs[0]["start"] == 0
        assert evd.env.collector.sfc_logs[0]["end"] == evd.env.now



class TestSFC:
    
    # Test SFC state is updated correctly for an input
    def test_update_state(self, sfc):
        sfc.start = 0
        sfc.end = 8
        sfc.max_delay = 10

        sfc._update_state()

        assert sfc.delay == sfc.end - sfc.start
        assert sfc.successful

    #  Test SFC comparison
    def test_comparability(self, sfc):
        sfc_1 = sfc
        sfc_2 = copy.deepcopy(sfc_1)

        sfc_1.spawn_time = 0
        sfc_2.spawn_time = 1

        assert sfc_1 < sfc_2

    # Test SFC accepts setting new attributes
    def test_sfc_sets_new_attrs(self):
        
        sfc = SFC("sfc_graph", sfc_id="sfc_id")

        assert sfc.sfc_id == "sfc_id"
