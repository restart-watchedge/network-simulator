import sys
sys.path.append("simulator/src")
import pytest
from unittest.mock import Mock
import json
import networkx as nx
from simulator.rng import RNG
from simulator.sfc import SFC
from simulator.environment import EnhancedEnvironment
from simulator.network import NetworkParser
from simulator.resource_models import BaseResourceModel
from simulator.algorithm import BaseAlgorithm
from env_test import NETWORK_TOPO_FILE, PARSED_SFC_FILE, SAMPLE_CONFIG


# Fixture to load configuration data from a JSON file.
@pytest.fixture
def config():
    with open(SAMPLE_CONFIG) as json_file:
        config_data = json.load(json_file)
    return config_data

# Fixture to create an EnhancedEnvironment instance based on the loaded configuration.
@pytest.fixture
def env(config):
    rng = RNG(42)
    env = EnhancedEnvironment(config, rng)
    env.build_network_from_file(NETWORK_TOPO_FILE)
    return env

# Fixture to parse the network topology from a file and return it as a NetworkX graph.
@pytest.fixture
def network(env):
    network_parser = NetworkParser(env)
    return network_parser.build_network_from_file(NETWORK_TOPO_FILE)

# Fixture to load network topology data from a JSON file.
@pytest.fixture
def network_json():
    with open(NETWORK_TOPO_FILE) as json_file:
        network_json = json.load(json_file)
    return network_json

# Fixture to create an SFC (Service Function Chain) instance based on loaded sample data.
@pytest.fixture
def sfc(env, network):
    with open(PARSED_SFC_FILE) as json_file:
        sample_sfc_json = json.load(json_file)
    sample_sfc_graph = nx.node_link_graph(sample_sfc_json, directed=True, multigraph=False) # to have sfc as nx.DiGraph
    mock_rm = Mock(spec=BaseResourceModel)
    mock_rm.calculate_delay.return_value = 100
    sfc = SFC(sample_sfc_graph, resource_model=mock_rm, sfc_id="test", placement_successful=False)
    return sfc

# Fixture to define a test algorithm for placement of SFCs.
@pytest.fixture
def algorithm():
    class TestAlgorithm(BaseAlgorithm):
        def place_sfc(self, sfc):
            node_list = ["edge_1_1", "edge_2_1", "cloud_3_1"]
            i=0
            for function in sfc.graph.nodes.values():
                    function["parameters"].update({
                        "network_id": node_list[i % len(node_list)],
                        "resource_id": "computational_resource"
                    })
                    i+=1
            return sfc
    return TestAlgorithm() 

# Fixture to provide link parameters for testing.
@pytest.fixture
def link_param():
    return {
        "resource_id": "tunnel",
        "request_timeout": 0,
        "requested_capacity": 10
        }
