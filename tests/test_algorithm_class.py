import sys
sys.path.append("algorithm/src")
import pytest
import networkx as nx
import json
from conftest import network_json, sfc, algorithm

class TestAlgorithmClass:
        
    # Test algorithm can load a network from json format
    def test_load_network(self, network_json, algorithm):
        algorithm = algorithm
        algorithm.load_network(network_json)

        assert isinstance(algorithm.network, nx.DiGraph)
