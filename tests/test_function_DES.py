import sys
sys.path.append("simulator/src")
from schema import Schema, SchemaError, And, Or
import networkx as nx
import numpy as np
import pytest
import json
from simulator.events import BaseEvent
from simulator.resources import NodeResource, LinkResource
from conftest import env, network, sfc

class TestTaskEvent:
    
    def create_resource_parameters(self, env, capacity=100):
        return {
            "env": env,
            "resource_host": "node",
            "network_id": "test_node",
            "resource_id": "test_resource",
            "capacity": capacity,
            "type": "iot",
            "f": 3e9
        }

    def create_event_parameters(self, env, sfc, event_id="1", requested_capacity=50):
            return {
                "env": env,
                "event_id": event_id,
                "event_type": "task",
                "task_type": "training",
                "requested_capacity": requested_capacity,
                "request_timeout": 0,
                "sfc": sfc,
                "image_count": 2
            }

    # Processed task results in corresponding flag
    def test_task_is_processed(self, sfc, env):
        param_res = self.create_resource_parameters(env)
        param_event = self.create_event_parameters(env, sfc)
        resource = NodeResource(**param_res)
        param_event["target_resource"] = resource
        task = BaseEvent(**param_event)
        env.process(task.process())
        env.run()

        assert task.successful

    # Task failed to process results in corresponding flag
    def test_task_is_not_processed(self, sfc, env):
        def delay_event(env, delay, resource, requested_capacity):
            with resource.resource.get(requested_capacity) as req:
                yield env.timeout(delay)
        delay = 3
        param_res = self.create_resource_parameters(env)
        param_event = self.create_event_parameters(env, sfc, requested_capacity=60)
        resource = NodeResource(**param_res)
        param_event["target_resource"] = resource
        task = BaseEvent(**param_event)

        env.process(delay_event(env, delay, resource, param_event["requested_capacity"]))
        env.process(task.process())
        env.run()

        assert not task.successful
        assert task.request_timeout == 0
        assert env.now >= task.request_timeout

    # A processed task should not have any np.infs anymore
    def test_processed_task_has_no_npinf(self, sfc, env):
        param_res = self.create_resource_parameters(env)
        param_event = self.create_event_parameters(env, sfc)
        resource = NodeResource(**param_res)
        param_event["target_resource"] = resource
        task = BaseEvent(**param_event)
        env.process(task.process())
        env.run()

        task_attr = task.__dict__

        assert task.successful
        for attr_value in task_attr.values():
                assert attr_value != np.inf

    # Task failed to process results in corresponding flag
    def test_task_waits_for_predecessors(self, sfc, env):
        def predecessor(env, delay):
            yield env.timeout(delay)
        delay = 3
        param_res = self.create_resource_parameters(env, capacity=100)
        param_event = self.create_event_parameters(env, sfc, requested_capacity=60)
        resource = NodeResource(**param_res)
        param_event["target_resource"] = resource

        task = BaseEvent(**param_event)

        sfc.schedule = nx.DiGraph()
        sfc.schedule.add_node(1, event_obj=env.process(predecessor(env, delay)))
        sfc.schedule.add_node(2, event_obj=env.process(predecessor(env, delay - 1))) # reduce delay by 1 to have different delays
        sfc.schedule.add_node(task.event_id, event_obj=env.process(task.process()))
        sfc.schedule.add_edge(1, task.event_id)
        sfc.schedule.add_edge(2, task.event_id)

        env.run()

        # check all predecessors successful
        for pred in sfc.schedule.predecessors(task.event_id):
            assert sfc.schedule.nodes[pred]["event_obj"].processed
        # check task actually starts after the biggest delay
        start_delay = task.real_delay - task.calculated_delay
        assert start_delay == delay

    # Test task interruption works
    def test_task_is_interrupted(self, sfc, env):
        def interrupt_callback(task, delay):
            yield task.env.timeout(delay)

        def interrupt(env, task, delay):
            yield env.timeout(0.1)
            task.waiting.interrupt({"method": interrupt_callback, "args": [task, delay], "kwargs":{}})
        interrupt_delay = 3
        param_res = self.create_resource_parameters(env)
        param_event = self.create_event_parameters(env, sfc)
        resource = NodeResource(**param_res)
        param_event["target_resource"] = resource
        task = BaseEvent(**param_event)
        env.process(task.process())
        env.process(interrupt(env, task, interrupt_delay))
        env.run()

        assert task.interrupted == True
        assert task.real_delay == interrupt_delay + task.calculated_delay

    # Test interruption callback is executed and can access properties of target task
    def test_task_interruption_callback(self, sfc, env):
        def interrupt_callback(task, delay, test_string):
            task.computation_effort = test_string
            yield task.env.timeout(delay)
        
        def interrupt(task, delay, new_id):
            yield task.env.timeout(0.1)
            task.waiting.interrupt({"method": interrupt_callback, "args": [task, delay, test_string], "kwargs":{}})

        interrupt_delay = 3
        test_string = "interrupted"
        param_res = self.create_resource_parameters(env)
        param_event = self.create_event_parameters(env, sfc)
        resource = NodeResource(**param_res)
        param_event["target_resource"] = resource
        task = BaseEvent(**param_event)
        env.process(task.process())
        env.process(interrupt(task, interrupt_delay, test_string))
        env.run()

        assert task.interrupted == True
        assert task.computation_effort == test_string
        assert task.real_delay == interrupt_delay + task.calculated_delay

class TestLinkEvent:
    
    def create_resource_parameters(self, env, bandwidth=100):
        return {
            "env": env,
            "resource_host": "link",
            "network_id": ["test_source", "test_target"],
            "resource_id": "test_resource",
            "type": "iot",
            "capacity": bandwidth
        }

    def create_event_parameters(self, env, sfc, event_id="1", requested_capacity=40):
            return {
                "env": env,
                "event_id": event_id,
                "event_type": "link",
                "requested_capacity": requested_capacity,
                "request_timeout": 0,
                "sfc": sfc,
                "data_size": 120
            }

    # Test resource is correctly blocked and released
    def test_resource_blocked_released_if_available(self, sfc, env):
        param_res = self.create_resource_parameters(env)
        param_event = self.create_event_parameters(env, sfc)
        resource = LinkResource(**param_res)
        param_event["target_resource"] = resource
        transmission = BaseEvent(**param_event)
        env.process(transmission.block_resource())
        env.run()
        assert resource.resource.level == param_res["capacity"] - param_event["requested_capacity"]
        assert len(resource.users) == 1
        assert resource.users == [param_event["event_id"]]

        env.process(transmission.release_resource())
        env.run()
        assert resource.resource.level == param_res["capacity"] 
        assert len(resource.users) == 0


    # Test resource is not blocked if not available and flag is set
    def test_resource_not_blocked_if_unavailable(self, sfc, env):
        param_res = self.create_resource_parameters(env, bandwidth=60)
        param_event = self.create_event_parameters(env, sfc)
        param_event_fail = self.create_event_parameters(env, sfc, event_id="fail")
        resource = LinkResource(**param_res)
        param_event["target_resource"] = resource
        param_event_fail["target_resource"] = resource
        transmission = BaseEvent(**param_event)
        transmission_fail = BaseEvent(**param_event_fail)
        env.process(transmission.process())
        env.process(transmission_fail.process())
        env.run()
        assert transmission.successful
        assert not transmission_fail.successful

    # Processed link transmission event results in corresponding flags and resource is released
    def test_link_event_is_processed(self, sfc, env):
        param_res = self.create_resource_parameters(env)
        param_event = self.create_event_parameters(env, sfc)
        resource = LinkResource(**param_res)
        param_event["target_resource"] = resource
        transmission = BaseEvent(**param_event)
        env.process(transmission.process())
        env.run()

        correct_transmission_time = sfc.resource_model.calculate_delay(**param_event)
        assert transmission.successful
        assert resource.resource.level == param_res["capacity"]
        assert transmission.calculated_delay == correct_transmission_time
        assert transmission.real_delay == correct_transmission_time
        assert env.now == correct_transmission_time
