
import math
import networkx as nx
import numpy as np
from simulator.algorithm import BaseAlgorithm



class BalancedRouting(BaseAlgorithm):
    """Balanced Routing Algorithm

    This class implements the Balanced Routing algorithm for placing service function chains (SFCs) in a network.

    Attributes:
        edge_nodes (list): A list of edge nodes in the network.
        cloud_nodes (list): A list of cloud nodes in the network.

    Methods:
        place_sfc(sfc): Place an SFC in the network using the Balanced Routing algorithm.
        _run_algorithm(sfc): Run the Balanced Routing algorithm for SFC placement.
        _route_clients(G): Route clients to servers based on network paths.
        _calculate_path(G, source, target, method="balanced", weight="utilization"): Calculate a network path.
        load_network(network_topo): Load the network topology.
        _prepare_network(): Prepare the network by initializing capacities and utilization.
        _update_network_status(): Update the network status based on resource utilization.
        _extract_clients(G): Extract client nodes from the SFC graph.
        _extract_servers(G): Extract server nodes from the SFC graph.
        _extract_en(): Extract edge nodes from the network.
        _extract_cn(): Extract cloud nodes from the network.
    """
    def __init__(self):
        super().__init__()
        self.edge_nodes = None
        self.cloud_nodes = None

    def place_sfc(self, sfc):
        """Place an SFC using the Balanced Routing algorithm.

        Args:
            sfc (SFC): The service function chain to be placed.

        Returns:
            SFC: The service function chain with tasks placed in the network.
        """
        self._extract_clients(sfc)
        self._extract_servers(sfc)
        self._update_network_status()
        return self._run_algorithm(sfc)

    def _run_algorithm(self, sfc):
        """Run the Balanced Routing algorithm for SFC placement.

        Args:
            sfc (SFC): The service function chain to be placed.

        Returns:
            SFC: The service function chain with tasks placed in the network.
        """
        return self._route_clients(sfc)

    def _route_clients(self, G):
        """Route clients to servers based on network paths.

        Args:
            G (nx.DiGraph): The SFC graph.

        Returns:
            nx.DiGraph: The SFC graph with clients routed to servers.
        """
        server = self.servers[0]
        server_loc = G.nodes[server]["parameters"]["network_id"]
        # For every client, find the shortest balanced path to the server
        for client in self.clients:
            client_loc = G.nodes[client]["parameters"]["network_id"]
            network_path = self._calculate_path(self.network, client_loc, server_loc)
            G.add_edge(client, server, parameters=self._generate_transmission_parameters(network_path, G=G))
        return G

    def _calculate_path(self, G, source, target, method="balanced", weight="utilization"):
        """Calculate a network path.

        Args:
            G (nx.DiGraph): The network topology.
            source (str): The source node ID.
            target (str): The target node ID.
            method (str, optional): The routing method ("balanced" or "shortest"). Defaults to "balanced".
            weight (str, optional): The edge weight attribute to consider. Defaults to "utilization".

        Returns:
            list: The calculated network path as a list of node IDs.
        """
        if method == "balanced":
            return nx.shortest_path(G, source, target, weight=weight)
        elif method == "shortest":
            return nx.shortest_path(G, source, target)
        else:
            pass
    
    def load_network(self, network_topo):
        """Load the network topology.

        Args:
            network_topo (dict): The network topology data in dictionary format.
        """
        self.network = self.json2graph(network_topo)
        self._prepare_network()
        self._extract_en()
        self._extract_cn()

    def _prepare_network(self):
        """Prepare the network by initializing capacities and utilization."""
        for link_id, link in self.network.edges.items():
            self.network.edges[link_id]["utilization"] = 0 
            self.network.edges[link_id]["available_capacity"] = 1000
        for node_id, node in self.network.nodes.items():
            self.network.nodes[node_id]["utilization"] = 0
            self.network.nodes[node_id]["available_capacity"] = 1000

    def _update_network_status(self):
        """Update the network status based on resource utilization."""
        for node_id, node in self.network_status.nodes.items():
            self.network.nodes[node_id]["utilization"] = node["computational_resource"]["current_occupation_rel"]
            self.network.nodes[node_id]["available_capacity"] = node["computational_resource"]["current_available_capacity"]
        for link_id, link in self.network_status.edges.items():
            self.network.edges[link_id]["utilization"] = link["tunnel"]["current_occupation_rel"] 
            self.network.edges[link_id]["available_capacity"] = link["tunnel"]["current_available_capacity"] 
    
    def _extract_clients(self, G):
        """Extract client nodes from the SFC graph.

        Args:
            G (nx.DiGraph): The SFC graph.
        """
        self.clients = [task_id for task_id, task in G.nodes.items() if task["parameters"]["task_type"] == "training"]

    def _extract_servers(self, G):
        """Extract server nodes from the SFC graph.

        Args:
            G (nx.DiGraph): The SFC graph.
        """
        self.servers = [task_id for task_id, task in G.nodes.items() if task["parameters"]["task_type"] == "server"]

    def _extract_en(self):
        """Extract edge nodes from the network.
        """
        self.edge_nodes = [node_id for node_id, node in self.network.nodes.items() if node["type"] == "edge"]
    
    def _extract_cn(self):
        """Extract cloud nodes from the network.
        """
        self.cloud_nodes = [node_id for node_id, node in self.network.nodes.items() if node["type"] == "cloud"]