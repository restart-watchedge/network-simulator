from simulator.simulation import Simulation
from simulator.environment import EnhancedEnvironment
from simulator.event_dispatcher import EventDispatcher
from sample_sim.sample_service import SampleService
from sample_sim.network_resource_modelling import NetworkResourceModelling
from simulator.utils import read_json
import logging

# Get the logger specified in the file
logger = logging.getLogger(__name__)

class SampleSim(Simulation):

    def __init__(self, config_file):
        super().__init__(config_file)
    
    def build_environment(self, network_topo_file):
        self.env = EnhancedEnvironment(self.config, self.rng)
        self.collector = self.env.collector
        self.env.build_network_from_file(network_topo_file)
        self.event_dispatcher = EventDispatcher(self.env, self.algorithm)
        self.service = SampleService(self.env, NetworkResourceModelling)
        # Pass network as json to algorithm
        network_json = read_json(network_topo_file)
        self.algorithm.load_network(network_json)
