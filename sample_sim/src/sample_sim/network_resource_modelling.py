from simulator.resource_models import BaseResourceModel
import math

class NetworkResourceModelling(BaseResourceModel):

    def __init__(self, env, config):
        self.env = env
        self.config = config

    def calculate_delay(self, event_type, **kwargs):
        if event_type == "task":
            return self._node_delay(**kwargs)
        elif event_type == "link":
            return self._link_delay(**kwargs)

    def _link_delay(self, **kwargs):
        return self.env.rng.uniform(10,100)
    
    def _node_delay(self, **kwargs):
        return self.env.rng.uniform(100,200)