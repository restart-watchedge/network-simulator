import numpy as np
import networkx as nx
import math
from simulator.services import BaseService
from simulator.sfc import SFC


class SampleService(BaseService):
    """A service simulating a sample application.

    This class represents a service in the simulation environment for a sample application.
    It is responsible for generating and managing SFC (Service Function Chain) requests for the
    sample application type. This service creates training tasks and a server task for each SFC request.

    Args:
        env (simpy.Environment): The simulation environment.
        resource_model (BaseResourceModel): The resource model for this service.

    Attributes:
        env (simpy.Environment): The simulation environment.
        network (nx.DiGraph): The network topology.
        application_type (str): The type of the application.
        config (dict): Configuration parameters for the sample service.
        resource_model (BaseResourceModel): The resource model for this service.
        rng (RNG): Random number generator for generating random values.
        clients (list): List of client nodes in the network.
        servers (list): List of server nodes in the network.

    Methods:
        new_sfc(): Generates a new SFC request based on the configuration.
        next_sfc_request(): Waits until the next SFC request must be deployed.
        _choose_clients(): Chooses a random number of clients from the list of clients.
        _choose_server_loc(): Chooses the location of the server.
        _add_sfc_graph(sfc_id, target_clients, server_loc): Adds tasks to the SFC graph.
        _add_tasks(G, sfc_id, target_clients, server_loc): Adds server and training tasks to the graph.
        _generate_training_parameters(sfc_id, event_id, target): Generates parameters for a training task.
        _generate_server_parameters(sfc_id, event_id, target): Generates parameters for a server task.
        _get_clients(): Retrieves the list of client nodes from the network.
        _get_servers(): Retrieves the list of server nodes from the network.
    """

    def __init__(self, env, resource_model):
        self.env = env
        self.network = env.network
        self.application_type = "sample_application"
        self.config = self.env.config[self.application_type]
        self.resource_model = resource_model(env, self.config)
        self.rng = self.env.rng
        self._get_clients()
        self._get_servers()

    def new_sfc(self):
        sfc_id = "sfc_" + str(np.around(self.env.now))
        target_clients = self._choose_clients()
        server_loc = self._choose_server_loc()
        sfc_graph = self._add_sfc_graph(sfc_id, target_clients, server_loc)
        
        sfc = SFC(sfc_graph, resource_model=self.resource_model, sfc_id=sfc_id, training_devices=len(target_clients), server_loc=server_loc, placement_successful=False)
        sfc.max_delay = self.config["sfc_max_delay"]
        sfc.spawn_time = self.env.now
        return sfc

    def next_sfc_request(self):
        timeout = self.rng.get(**self.config["generation_interval"])
        yield self.env.timeout(timeout)
    
    def _choose_clients(self):
        client_amount = self.rng.get(**self.config["client_amount"]["distribution"])
        return self.rng.sample(self.clients, client_amount)

    def _choose_server_loc(self):
        # there is only one server (also named cloud) location
        return self.servers[0]

    def _add_sfc_graph(self, sfc_id, target_clients, server_loc):
        sfc_graph = nx.DiGraph()
        sfc_graph = self._add_tasks(sfc_graph, sfc_id, target_clients, server_loc)
        return sfc_graph

    def _add_tasks(self, G, sfc_id, target_clients, server_loc):
        # add server task
        server_id = "server" + str(0)
        G.add_node(server_id, parameters=self._generate_server_parameters(sfc_id, server_id, server_loc))
        task_count = 0
        # add predecessing training tasks
        for client in target_clients:
            event_id = "client" + str(task_count)
            G.add_node(event_id, parameters=self._generate_training_parameters(sfc_id, event_id, client))
            G.add_edge(event_id, server_id)
            task_count += 1
        return G

    def _generate_training_parameters(self, sfc_id, event_id, target):
        parameters = {
            "application_type": self.application_type,
            "sfc_id": sfc_id,
            "event_id": event_id,
            "network_id": target,
            "resource_id": "computational_resource",
            "task_type": "training",
            "image_count": int(self.rng.get(**self.config["client_data"])),
            "request_timeout": 1e9,
            "requested_capacity": int(self.rng.get(**self.config["train_cpu"]))
        }
        return parameters

    def _generate_server_parameters(self, sfc_id, event_id, target):
        parameters = {
            "application_type": self.application_type,
            "sfc_id": sfc_id,
            "event_id": event_id,
            "network_id": target,
            "resource_id": "computational_resource",
            "task_type": "server",
            "request_timeout": 0,
            "requested_capacity": int(self.rng.get(**self.config["server_cpu"]))
        }
        return parameters

    def _get_clients(self):
        self.clients = [node_id for node_id, node in self.network.nodes.items() if node["type"] == "iot"]

    def _get_servers(self):
        self.servers = [node_id for node_id, node in self.network.nodes.items() if node["type"] == "cloud"]