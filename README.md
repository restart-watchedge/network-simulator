# Network Simulator
To cite this work please use the following: 

N. Ploch, S. Troia, W. Kellerer and G. Maier, “Edge-to-Cloud Federated Learning with Resource-Aware Model Aggregation in MEC”, IEEE International Conference on Communication Workshops (ICC Workshops), Denver, USA, 2024.

# Description
The rapid increase in the number of connected devices paired with the adoption of Machine Learning (ML) applications dramatically augments the computation and communication requirements imposed on today’s telecommunication networks. New ML techniques and networking paradigms such as Federated Learning (FL), Multi-access Edge Computing (MEC), and Software-Defined Wide Area Networks (SD-WANs) are needed to cope with these requirements. However, to run FL in MEC SD-WANs, intelligent resource management strategies and an evaluation of the impact of FL on the network resources are necessary.

To study the given problem and finding our solution, we developed a resource allocation simulator in a networking context (main branch of this repo) that we tailored to our specific use case (implementation-icc-2024-cloudb5g). The simulator aims at facilitating the evaluation of resource allocation algorithms for distributed machine learning (DML) techniques, such as Federated Learning, in the context of multi-access edge computing (MEC) networks. 

The simulator is able to simulate the resource usage of applications in a given network using discrete-event simulation (DES). For this, simulation events temporarily block resources in the network following defined latency models. The simulation events are grouped in directed acyclic graphs, thus being able to express task dependencies and model complex applications. In this simulation environment, we call the applications Service Function Chains (SFCs). The main packages used are `networkx` for network simulation and `simpy` for DES, where objects from the `simpy` package are added to a graph provided by `networkx`.

For more details on the first use-case, usage, and theoretical base of the simulator please refer to the publication and the thesis manuscript (contact any of the authors).

# Repository Structure
* simulator/src/simulator/ : contains the code of the simulator
* sample_sim/src/sample_sim/ : incldues a sample implementation of the simulator, containing a sample service sending requests, a network resource model, and two routing algorithms. 
* tests/ : includes the tests for the simulator
* experiments/ : used for logging experiments with the simulator
  * playground/ : jupyter notebook and simulator configuration to get started
  * utils/ : useful notebooks to use along with the simulator, including topology generation and testing algorithm logics
* external_resources/ : includes sample configuration files, network topologies, and test configurations used by the tests 

# Installation
To run the simulator, you will need to clone the repository, have a running docker environment and docker compose installed. Follow these steps:

1. Clone the repository and navigate to the source folder of the repo
2. In the docker-compose.yml file, check if the settings for resources are adequate for your system
3. Run `docker-compose up`. This starts a jupyter server.

# Usage
To use the simulator, you can use the link provided in the terminal output of docker-compose to visit the jupyter server. Alternatively, you can run a script within the container by executing `docker exec -it simulator /bin/bash`, navigating to your designated folder and executing the script with `python -m myscript`.

To run a simulation, the following minimum components must be implemented:
* A service generating SFCs inheriting the API of `src/simulator/services.py`.
* A resource model inheriting the API of `src/simulator/resource_models.py`.
* An algorithm placing SFC requests that inherits the API from `src/simulator/algorithm.py`.
* A JSON-file containing the network to be simulated. You can generate a network with `src/simulator/topology_gen.py`
* A JSON configuration file containing the simulation parameters. Please see an exemplary configuration file in the playground branch. 
* A simulation object inheriting the API from `src/simulator/simulation.py`. Be careful to implement your own `build_environment()` method to include your own service and resource model.

With these components implemented, it is possible to run a simulation with the following steps:
```
sim = YourSimulationObject("./your_config.json")
sim.set_algorithm(YourAlgorithm) # algorithm object must not be initialized
sim.build_environment("path_to_your_json_topo")
sim.run_simulation() # stop after an amount of requests generated that is specified in the configuration file
```

The reports at the end of the simulation can be extracted as pd.DataFrame as follows:
```
# includes information about SFC requests, e.g. whether an SFC request was successful or not
sfc_report = sim.collector.get_sfc_report() 

# includes information wrt network resources, e.g. available capacity at a timestamp
res_report = sim.collector.get_resource_report()

# includes further runtime information, e.g. placement time for a request by an algorithm 
rt_report = sim.collector.get_runtime_report() 

# includes event-related information
task_report = sim.collector.get_event_report() 
```

Each simulation use case requires its own reporting. For further details on reporting for a specific implementation, the playground branch may help out.

## Extendability
It is also possible to extend other components of the simulator, such as the resource allocator object, the event dispatcher, the base resource object or the base event object as long as they inherit from the class provided in the simulator code.

# Contributing guidelines
Before deciding how to contribute, think if your contribution is meant for the whole simulator or only a feature. If it is a feature, please consider creating a feature branch to add your feature. Be sure the pipeline runs through. To contribute to the simulator, please follow these steps: 
1. create a new issue
2. add a new branch and merge request. Name convention is: #task_number-task-name
3. if additional testing is performed, e.g. in jupyter notebooks, provide the information in the merge request
4. only merge if the pipelines run successfully

# Support
Please find detailed video tutorials for the simulator under: https://polimi365-my.sharepoint.com/:f:/g/personal/10377693_polimi_it/EnUE9SHYnj1PjQNNxJ_cATkBaBYDX2AN86KnCld0SQqSKg?e=xOQ2HP

For issues, please contact sebastian.troia@polimi.it or noah.ploch@tum.de

# Authors and acknowledgment
The main contributor of this project was Noah Ploch (noah.ploch@tum.de).
This project was conducted under supervision of Sebastian Troia (sebastian.troia@polimi.it), Prof. Dr.-Ing. Wolfgang Kellerer (wolfgang.kellerer@tum.de), and Guido Maier (guido.maier@polimi.it).

# License
The code is released under the GNU General Public License.
