import simpy
import networkx as nx
import pandas as pd
import numpy as np
from simulator.algorithm import BaseAlgorithm
from simulator.environment import EnhancedEnvironment
from simulator.event_dispatcher import EventDispatcher
from simulator.services import BaseService
from simulator.resource_models import BaseResourceModel
from simulator.rng import RNG
from simulator.utils import read_json



import logging
from pathlib import Path
import os, sys
import logging.config
from datetime import datetime

# configure logger
now = datetime.now()
log_info = now.strftime("%Y-%m-%d_%H-%M-%S")
filename = sys.path[0].split('/')[-1] # get the name of the folder where the simulation is currently executed in
if not os.path.exists(os.path.join(sys.path[0], 'logs')):
    os.mkdir(os.path.join(sys.path[0], 'logs'))
logging.config.fileConfig(fname=Path(__file__).with_name('logging.conf'), disable_existing_loggers=False, defaults={'log_filename': os.path.join(sys.path[0], 'logs', log_info + '_' + filename + '.log')})


# Get the logger specified in the file
logger = logging.getLogger(__name__)

class Simulation:
    """Simulation represents a simulation environment for evaluating Service Function Chain (SFC) algorithms.

    Args:
        config_file (str): The path to the configuration file for the simulation.

    Attributes:
        config (dict): The configuration settings for the simulation.
        rng (RNG): The random number generator for the simulation.
        algorithm (BaseAlgorithm): The algorithm used for SFC placement.
        env (EnhancedEnvironment): The simulation environment.
        collector: The data collector for logging simulation metrics.
        event_dispatcher (EventDispatcher): The event dispatcher for managing SFC events.
        service (BaseService): The service for generating SFC requests.

    Methods:
        set_algorithm(algorithm=None, config=None):
            Set the algorithm for SFC placement.

        build_environment(network_topo_file):
            Build the simulation environment and network topology.

        provide_sfc(training_rounds=None):
            Provide SFC requests to the simulation.

        terminate(service):
            Terminate the simulation when all services are deployed.

        run_simulation(runtime=None):
            Run the SFC simulation for a specified runtime or training rounds.

    """
    def __init__(self, config_file):
        self.config = read_json(config_file)
        self.rng = RNG(self.config["simulation"]["seed"])

    def set_algorithm(self, algorithm=None, config=None):
        """Set the algorithm for SFC placement.

        Args:
            algorithm (class, optional): The algorithm class to use. Defaults to None.
            config (dict, optional): Configuration settings for the algorithm. Defaults to None.
        """
        self.algorithm = algorithm()
        if not config:
            config = self.config["algorithm"]
        self.algorithm.setup_algorithm(config)
    
    def build_environment(self, network_topo_file):
        """Build the simulation environment and network topology.

        Args:
            network_topo_file (str): The path to the network topology file.
        """
        self.env = EnhancedEnvironment(self.config, self.rng)
        self.collector = self.env.collector
        self.set_algorithm(BaseAlgorithm)
        self.env.build_network_from_file(network_topo_file)
        self.event_dispatcher = EventDispatcher(self.env, self.algorithm)
        self.service = BaseService(self.env, BaseResourceModel)
        # Pass network as json to algorithm
        network_json = read_json(network_topo_file)
        self.algorithm.load_network(network_json)

    def provide_sfc(self, training_rounds=None):
        """Provide SFC requests to the simulation.

        Args:
            training_rounds (int, optional): The number of training rounds. Defaults to None.

        Yields:
            SFC: The generated SFC request.
        """
        if not training_rounds:
            while True:
                sfc = self.service.new_sfc()
                yield self.env.sfcs.put(sfc)
                yield self.env.process(self.service.next_sfc_request())
        else:
            for _ in range(training_rounds):
                sfc = self.service.new_sfc()
                yield self.env.sfcs.put(sfc)
                yield self.env.process(self.service.next_sfc_request())

    def terminate(self, service):
        """Terminate the simulation when all services are deployed.

        Args:
            service: The service process.

        Yields:
            simpy.AllOf: An event representing the termination of the simulation.
        """
        yield service
        yield simpy.AllOf(self.env, self.event_dispatcher.deployed_sfcs)
    
    def run_simulation(self, runtime=None):
        """Run the SFC simulation for a specified runtime or training rounds.

        Args:
            runtime (float, optional): The simulation runtime. Defaults to None.
        """
        logger.info("Running SFC simulation...")
        self.env.process(self.event_dispatcher.dispatch())
        if runtime:
            self.env.process(self.provide_sfc())
            self.env.run(until=runtime)
        else:
            service = self.env.process(self.provide_sfc(self.env.config["simulation"]["requests"]))
            terminate = self.env.process(self.terminate(service))
            self.env.run(until=terminate)
        logger.info("Finished simulation!")
