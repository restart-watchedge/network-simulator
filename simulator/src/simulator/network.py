import networkx as nx
import json
from simulator.resources import NodeResource, LinkResource
import logging

# Get the logger specified in the file
logger = logging.getLogger(__name__)


class NetworkParser():
    """Parser for building and processing network topologies in simulation.

    This class provides methods for reading network topology data from files, adding resource objects to nodes and links,
    and converting network data to different formats.

    Args:
        env (simpy.Environment): The simulation environment where the network will be used.

    """
    
    def __init__(self, env):
        self.env = env
        
    def build_network_from_file(self, filename):
        """Build a network graph from a file.

        This method reads network topology data from a JSON or GML file, creates a network graph, and adds resource objects
        to nodes and links.

        Args:
            filename (str): The path to the network topology file.

        Returns:
            networkx.Graph: The network graph with added resource objects.

        """
        logger.info("Building network...")
        return self._add_resources_to_network(self._read_network_from_file(filename))
        
    def convert_json_to_gml(self, source_file, target_file):
        """Convert a JSON file to GML format.

        This method reads a network topology from a JSON file and converts it to the GML format, saving it to the specified
        target file.

        Args:
            source_file (str): The path to the source JSON file.
            target_file (str): The path to the target GML file where the converted network will be saved.

        """
        imported_graph = self._read_network_from_file(source_file)
        nx.write_gml(imported_graph, target_file)

    def _read_network_from_file(self, filename):
        """Read network data from a file (internal method).

        This method reads network topology data from either a JSON or GML file.

        Args:
            filename (str): The path to the network topology file.

        Returns:
            networkx.Graph: The network graph read from the file.

        Raises:
            TypeError: If the filename is not a string.
            NotImplementedError: If the file format is not supported (other than JSON and GML).

        """
        if not isinstance(filename, str):
            raise TypeError("Filename passed is not a string!")
        if not filename.endswith((".json", ".gml")):
            raise NotImplementedError("Reading network data from files other than JSON and GML is not supported!")
        if filename.endswith(".json"):
            return self.read_network_from_json(filename)
        elif filename.endswith(".gml"):
            return self._read_network_from_gml(filename)
        logger.info("Loaded network topology data from file!")
    
    def _add_resources_to_network(self, graph):
        """Add resource objects to nodes and links in the network.

        This method adds resource objects (NodeResource and LinkResource) to nodes and links in the network graph.

        Args:
            graph (networkx.Graph): The network graph.

        Returns:
            networkx.Graph: The network graph with added resource objects.

        """
        for node_id, node in graph.nodes.items():
            self._add_resources_to_node(node_id, node)
        logger.info("Added resource objects to nodes!")
        for link_id, link in graph.edges.items():
            self._add_resources_to_links(link_id, link)
        logger.info("Added resource objects to links!")
        return graph

    def _add_resources_to_node(self, node_id, node):
        """Add resource objects to a node.

        This method adds NodeResource objects to a node in the network graph.

        Args:
            node_id (str): The ID of the node.
            node (dict): The node data containing resource specifications.

        """
        node["resources"] = {}
        for resource_id, resource_specs in node["resource_specs"].items():
            resource_specs["env"] = self.env
            resource_specs["resource_host"] = "node"
            resource_specs["network_id"] = node_id
            resource_specs["resource_id"] = resource_id
            node["resources"][resource_id] = NodeResource(**resource_specs)

    def _add_resources_to_links(self, link_id, link):
        """Add resource objects to a link.

        This method adds LinkResource objects to a link in the network graph.

        Args:
            link_id (str): The ID of the link.
            link (dict): The link data containing resource specifications.

        """
        link["resources"] = {}
        for resource_id, resource_specs in link["resource_specs"].items():
            resource_specs["env"] = self.env
            resource_specs["resource_host"] = "link"
            resource_specs["network_id"] = link_id
            resource_specs["resource_id"] = resource_id
            link["resources"][resource_id] = LinkResource(**resource_specs)

    def read_network_from_json(self, filename):
        """Read network data from a JSON file.

        This method reads network topology data from a JSON file and returns a network graph.

        Args:
            filename (str): The path to the JSON file containing network topology data.

        Returns:
            networkx.Graph: The network graph read from the JSON file.

        """
        with open(filename) as json_file:
            network_data = json.load(json_file)
        logger.debug("Loading from JSON File...")
        return nx.node_link_graph(network_data, directed=True, multigraph=False)

    def _read_network_from_gml(self, filename):
        """Read network data from a GML file.

        This method reads network topology data from a GML file and returns a network graph.

        Args:
            filename (str): The path to the GML file containing network topology data.

        Returns:
            networkx.Graph: The network graph read from the GML file.

        """
        logger.debug("Loading from GML File...")
        return nx.read_gml(filename)

    def graph2json(self, G):
        """Convert a network graph to JSON format.

        This method converts a network graph to JSON format.

        Args:
            G (networkx.Graph): The network graph to be converted.

        Returns:
            dict: The network graph data in JSON format.

        """
        return nx.node_link_data(G)

