import simpy
import numpy as np
import logging

# Get the logger specified in the file
logger = logging.getLogger(__name__)

class BaseEvent:
    """Base class for modeling events in the simulator.

    Args:
        env (simpy.Environment): The simulation environment.
        event_id (string): Identifier for the event.
        target_resource (object): The resource that the event targets.
        requested_capacity (float): The capacity on the resource requested by the event.
        request_timeout (float): Timeout for the resource request.
        sfc (SFC): The service function chain to which the event belongs.
        **kwargs: Additional keyword arguments for custom attributes.
    
    Attributes:
        env: simulation environment
        event_id: event ID
        target_resource: resource object the event requests capacity from
        requested_capacity: capacity requested
        request_timeout: event fails if requested capacity is not allocated before timeout
        sfc: SFC object
        spawn_time: simulation timestamp when event has been deployed
        calculated_delay: delay calculated from resource model
        real_delay: delay considering waiting time for predecessors
        start: execution start of event
        end: execution end of event
        successful: whether event has been successfully executed
        interrupted: whether event has been interrupted
        waiting: sub-event representing waiting time during execution. Enables event interruption

    Methods:
        process(): Process the event.
        run_event(): Run the event.
        log_resource(log_iot=False): Log information of the targeted resource
        wait(): Wait for the defined event delay, with the possibility of event interruption following simpy package.
        update_sfc(): Update the service function chain the event is part of.
        wait_for_predecessing_tasks(): Wait for predecessor tasks to complete.
        block_resource(): Block the target resource for the event.
        release_resource(): Release the target resource.
        log(): Generate a log for the event.

    """
    
    def __init__(self, env, event_id, target_resource, requested_capacity, request_timeout, sfc, **kwargs):
        self.env = env
        self.event_id = event_id
        self.target_resource = target_resource
        self.requested_capacity = requested_capacity
        self.request_timeout = request_timeout
        self.sfc = sfc
        self.spawn_time = np.inf
        self.calculated_delay = np.inf
        self.real_delay = np.inf
        self.start = np.inf
        self.end = np.inf
        self.successful = False
        self.interrupted = False
        self.waiting = None
        for key, value in kwargs.items():
            if key not in self.__dict__:
                setattr(self, key, value)
    
    def process(self):
        """Process the event.

        This method processes the event, including waiting for predecessors,
        calculating delay, running the event, and logging it if configured.

        """
        if self.sfc.schedule:
            self.predecessor_count = len(list(self.sfc.schedule.predecessors(self.event_id)))
        else:
            self.predecessor_count = 1
        # if resources should not be preallocated yield predecessing tasks before reserving capacity 
        if not self.env.config["simulation"]["preallocate"]:
            if self.sfc.schedule:
                yield self.env.process(self.wait_for_predecessing_tasks())
        if not self.sfc.successful:
            return
        # calculate event delay based on resource model attached to SFC
        self.calculated_delay = self.sfc.resource_model.calculate_delay(**self.__dict__)
        yield self.env.process(self.run_event())
        self.update_sfc()
        # log events only if required (decreases performance and uses more memory)
        if self.env.config["simulation"]["log_events"]:
            self.env.collector.log_event(self.log())
        logger.debug(f"T {np.around(self.env.now, 4)} - {self.event_id} - Processed!")

    def run_event(self):
        """Run the event.

        This method runs the event, including event spawning, blocking the resource, waiting for task timeout,
        and updating event attributes.

        """
        self.spawn_time = float(self.env.now)
        logger.info(f"T {np.around(self.env.now, 4)} - {self.event_id} - spawned at {np.around(self.spawn_time, 4)} s")
        block = yield self.env.process(self.block_resource())
        if block:
            self.env.collector.log_runtime(self.env.now, "sfc_active", self.sfc.sfc_id)
            self.start = float(self.env.now)
            self.log_resource(self.env.config["simulation"]["log_iot"])
             # if resources should be preallocated event already blocks capacity during predecessor runtime
            if self.env.config["simulation"]["preallocate"]:
                if self.sfc.schedule:
                    yield self.env.process(self.wait_for_predecessing_tasks())
            # wait for task timeout with the possibility of interrupting
            self.waiting = self.env.process(self.wait())
            yield self.waiting
            self.end = float(self.env.now)
            self.real_delay = float(self.end - self.start)
            self.successful = True
            self.target_resource.total_tasks += 1
            self.log_resource(self.env.config["simulation"]["log_iot"])
        else:
            # update flags and metrics for task failure
            self.target_resource.failed_tasks += 1
            self.target_resource.total_tasks += 1
            self.log_resource(self.env.config["simulation"]["log_iot"])
            self.successful = False
            self.sfc.add_attribute("failed_at", self.target_resource.network_id)
            self.sfc.add_attribute("failed_at_type", self.target_resource.resource_host)
            self.sfc.add_attribute("failed_task", self.event_id)
            if hasattr(self, "task_type"):
                self.sfc.add_attribute("failed_task_type", self.task_type)
    
    def log_resource(self, log_iot=False):
        """Log resource-related information.

        Args:
            log_iot (bool): Whether to log IoT resources (default is False).

        """
        if not log_iot:
            if self.target_resource.type != "iot": # log only if not iot device/client/end device
                self.env.collector.log_resource(self.target_resource.log())
        else:
            self.env.collector.log_resource(self.target_resource.log())
    
    def wait(self):
        """Wait for a specified amount of time with possible interruption.

        This method waits for the calculated event delay while being ready for interruptions. For a more detailed
        explanation on interruptions, see https://simpy.readthedocs.io/en/latest/topical_guides/process_interaction.html#interrupting-another-process
        It handles any appended functions to the interrupt callback. This function allows to interrupt a running event to execute some function, e.g. a change of an event
        parameter.

        """
        curr_calculated_delay = float(self.calculated_delay)
        # try to process the tasks, but be ready for interrupts
        while curr_calculated_delay:
            try:
                yield self.env.timeout(curr_calculated_delay)
                yield self.env.process(self.release_resource())
                curr_calculated_delay = 0
            except simpy.Interrupt as i:
                if self.start == np.inf:
                    pass
                else:
                    self.interrupted = True
                    curr_calculated_delay -= self.env.now - self.start
                    # execute function appended to interruption
                    yield self.env.process(i.cause["method"](*i.cause["args"], **i.cause["kwargs"]))

    def update_sfc(self):
        """Update the service function chain.

        This method updates the service function chain by removing the finished task from the SFC graph
        and marking the entire SFC as unsuccessful if the task fails.

        """
        # Remove finished task from sfc graph
        if self.event_id in self.sfc.graph.nodes:
            self.sfc.graph.remove_node(self.event_id)
        # If a task fails, the whole sfc fails
        if self.successful:
            pass
        else:
            self.sfc.successful = False

    def wait_for_predecessing_tasks(self):
        """Wait for predecessor tasks to complete.

        This method waits for the completion of predecessor tasks in the schedule graph.

        """
        for pred in self.sfc.schedule.predecessors(self.event_id):
            yield self.sfc.schedule.nodes[pred]["event_obj"]
        logger.debug(f"T {np.around(self.env.now, 4)} - {self.event_id} - Processed predecessors {list(self.sfc.schedule.predecessors(self.event_id))}")
        
    def block_resource(self):
        """Block the resource for the event.

        This method attempts to block the resource for the event, considering the requested capacity
        and timeout.

        Returns:
            bool: True if the resource was successfully blocked, False otherwise.

        """
        req = self.target_resource.resource.get(self.requested_capacity)
        # wait for successful capacity request or request timeout 
        result = yield req | self.env.timeout(self.request_timeout)
        if req in result:
            self.target_resource.users.append(self.event_id)
            return True           
        else:
            logger.info(f"T {np.around(self.env.now, 4)} - {self.event_id} - resource {self.target_resource.network_id} - {self.target_resource.resource_id} not available: {self.requested_capacity} > {self.target_resource.resource.level}")
            self.target_resource.resource.get_queue.clear()
            return False

    def release_resource(self):
        """Release the resource.

        This method releases the resource after the event has finished.

        """
        self.target_resource.users.remove(self.event_id)
        # add blocked resources back to the container object     
        yield self.target_resource.resource.put(self.requested_capacity)

    def log(self):
        """Generate a log for the event.

        Returns:
            dict: A dictionary containing log information for the event.

        """
        log_dict = {"timestamp": self.env.now}
        log_dict.update(self.__dict__)
        keys_to_remove = ["env", "target_resource", "waiting", "sfc"]
        for key in keys_to_remove:
            del log_dict[key]
        return log_dict


