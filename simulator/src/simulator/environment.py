import numpy as np
import json

from simpy import Environment, PriorityStore
from simulator.collector import Collector
from simulator.network import NetworkParser

class EnhancedEnvironment(Environment):
    """An enhanced simulation environment with additional functionality.

    This class extends the simpy.Environment class to provide additional functionality for simulation.
    It includes a collector for logging simulation data, configuration settings, and a network parser.

    Args:
        config (dict): Configuration settings for the simulation.
        rng: Random number generator.

    Attributes:
        collector (Collector): An instance of the Collector class for collecting simulation data.
        config (dict): Configuration settings for the simulation.
        sfcs (PriorityStore): PriorityStore for managing Service Function Chains (SFCs).
        rng(RNG): Random number generator for generating random values.
        network_parser (NetworkParser): An instance of the NetworkParser class for parsing network topology.
        network: The network topology used in the simulation.

    """

    def __init__(self, config, rng):
        """Initialize an EnhancedEnvironment instance.

        Args:
            config (dict): Configuration settings for the simulation.
            rng: Random number generator.

        """
        super().__init__()
        self.collector = Collector()
        self.config = config
        self.sfcs = PriorityStore(self)
        self.rng = rng
        self.network_parser = NetworkParser(self)
        self.network = None

    def build_network_from_file(self, network_topo_file):
        """Build the network topology graph with node and link resources from a file.

        This method parses the network topology from a file and sets it as the network attribute.

        Args:
            network_topo_file (str): Path to the network topology file.

        """
        self.network = self.network_parser.build_network_from_file(network_topo_file)