# This code was taken from Analysis, Modeling and Simulation of Communication Networks Course @ Technical University Munich, Summer Semester 2021
# The supervisors of this course were Wolfgang Kellerer, Andreas Blenk, Alba Jano, and Alberto Martinez Alba
import math
import numpy
import scipy
import scipy.stats


class Counter(object):

    def __init__(self, name="default"):
        self.name = name
        self.values = []

    def count(self, *args):
        """
        Count values and add them to the internal array.
        Abstract method - implement in subclass.
        """
        raise NotImplementedError("Please Implement this method")

    def reset(self, *args):
        """
        Delete all values stored in internal array.
        """
        self.values = []

    def get_mean(self):
        """
        Returns the mean value of the internal array.
        Abstract method - implemented in subclass.
        """
        raise NotImplementedError("Please Implement this method")

    def get_var(self):
        """
        Returns the variance of the internal array.
        Abstract method - implemented in subclass.
        """
        raise NotImplementedError("Please Implement this method")

    def get_stddev(self):
        """
        Returns the standard deviation of the internal array.
        Abstract method - implemented in subclass.
        """
        raise NotImplementedError("Please Implement this method")

    def report(self):
        """
        Print report for this counter.
        """
        if len(self.values) != 0:
            print('Name: ' + str(self.name) + ', Mean: ' + str(self.get_mean()) + ', Variance: ' + str(self.get_var()))
        else:
            print("List for creating report is empty. Please check.")


class TimeIndependentCounter(Counter):
    def __init__(self, name="default"):
        super(TimeIndependentCounter, self).__init__(name)

    def count(self, *args):
        """Add a new value to the internal array.

        Parameters are chosen as *args because of the inheritance to the correlation counters.

        Args:
            *args: The value that should be added to the internal array.

        """
        self.values.append(args[0])

    def get_mean(self):
        """Return the mean value of the internal array.

        Returns:
            float: The mean value.

        """
        if len(self.values) > 0:
            return numpy.mean(self.values)
        else:
            return 0

    def get_var(self):
        """Return the variance of the internal array.

        Note that this method returns the estimated variance, not the exact variance.

        Returns:
            float: The variance.

        """
        return numpy.var(self.values, ddof=1)

    def get_stddev(self):
        """Return the standard deviation of the internal array.

        Returns:
            float: The standard deviation.

        """
        return numpy.std(self.values, ddof=1)

    def report_confidence_interval(self, alpha=0.05, print_report=True):
        """Report a confidence interval with a given significance level.

        This is done by using the t-table provided by scipy.

        Args:
            alpha (float): The significance level (default is 0.05).
            print_report (bool): Enable an output string (default is True).

        Returns:
            float: The half-width of the confidence interval.

        """
        n = len(self.values)
        mean = self.get_mean()
        var = self.get_var()
        h = math.sqrt(var / n) * scipy.stats.t.ppf(1 - alpha / 2., n - 1)

        if print_report:
            print('Counter: ' + str(self.name) + '; number of samples: ' + str(n) + '; mean: ' + str(
                mean) + '; var: ' + str(var) + '; confidence interval: [' + str(mean - h) + ' ; ' + str(
                mean + h) + '] (h/2: ' + str(h) + ')')

        return h

    def report_bootstrap_confidence_interval(self, alpha=0.05, resample_size=5000, print_report=True):
        """Report bootstrapping confidence interval with given significance level.

        This is done with the bootstrap method.

        Args:
            alpha (float): Significance level.
            resample_size (int): Resampling size.
            print_report (bool): Enable an output string (default is True).

        Returns:
            tuple: A tuple containing lower and upper bounds of the confidence interval.

        """
        n = len(self.values)
        mean = self.get_mean()
        var = self.get_var()

        means = []
        for i in range(resample_size):
            samples = numpy.random.choice(self.values, len(self.values), replace=True)
            means.append(numpy.mean(samples))

        # Percentile confidence intervals (option 1)
        lower_index, upper_index = int(alpha / 2 * resample_size), int(resample_size * (1 - alpha / 2))
        lower, upper = numpy.sort(means)[lower_index], numpy.sort(means)[upper_index]

        if print_report:
            print('Counter: ' + str(self.name) + '; number of samples: ' + str(n) + '; mean: ' + str(
                mean) + '; var: ' + str(var) + '; confidence interval: [' + str(lower) + ' ; ' + str(upper) + '] ')

        return lower, upper
