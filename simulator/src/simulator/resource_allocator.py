import simpy
import requests
import networkx as nx
import time



class ResourceAllocator:
    """ResourceAllocator is responsible for allocating network resources to service function chains.

    Args:
        env (simpy.Environment): SimPy environment representing the simulation environment.
        algorithm: The algorithm to use for resource allocation.

    Attributes:
        env (simpy.Environment): The SimPy environment in which the resource allocation is performed.
        network (nx.Graph): The network representation.
        network_status (nx.DiGraph): A directed graph representing the current status of network resources.
        mec_nodes (list): List of MEC (Multi-access Edge Computing) nodes in the network.
        mec_links (list): List of MEC links (edges) in the network.
        algorithm: The algorithm used for resource allocation.

    """

    def __init__(self, env, algorithm):
        self.env = env
        self.network = env.network
        self.network_status = nx.DiGraph()
        self._get_mec_links()
        self._get_mec_nodes()
        self.algorithm = algorithm

    def get_allocation(self, sfc):
        """Gets the allocation for a given service function chain from the algorithm.

        Args:
            sfc (SFC): An instance of the SFC class representing a service function chain.

        Returns:
            SFC: The service function chain with functions placed in the network.
        """
        self.update_network_status()
        start = time.time() # to calculate placement time by algorithm
        self.algorithm.network_status = self.network_status
        resp = self.algorithm.place_sfc(sfc.graph)
        end = time.time()
        if resp:
            sfc.graph = resp
            sfc = self._add_env_parameters(sfc)
            self.env.collector.log_runtime(self.env.now, "placement_time", end-start)
        else:
            sfc.successful = False
        return sfc
    
    def _add_env_parameters(self, sfc):
        """Adds environment parameters to the service function chain.

        Args:
            sfc (SFC): The service function chain.

        Returns:
            SFC: The updated service function chain with environment parameters added.
        """
        for function in sfc.graph.nodes.values():
                function["parameters"].update({
                    "env": self.env
                })
        return sfc

    def update_network_status(self):
        """Update the network status graph to be sent to the algorithm. 

        Returns:
            JSON: A JSON-formatted nx.DiGraph containing the updated network status
        """
        log_from_resource = ["current_occupation_rel", "current_available_capacity", "failed_tasks", "total_tasks"]
        # extract current network status from network object, but only for MEC nodes (disregard end devices!)
        for node_id in self.mec_nodes:
            resource_status = {}
            node = self.network.nodes[node_id]
            for resource_id, resource in node["resources"].items():
                logs, status = resource.log(log_from_resource)
                self.env.collector.log_resource(logs)
                resource_status[resource_id] = status
            self.network_status.add_node(node_id, **resource_status)
        for link_id in self.mec_links:
            resource_status = {}
            link = self.network.edges[link_id]
            for resource_id, resource in link["resources"].items():
                logs, status = resource.log(log_from_resource)
                self.env.collector.log_resource(logs)
                resource_status[resource_id] = status
            self.network_status.add_edge(*link_id, **resource_status)

    def _get_mec_nodes(self):
        """Identify MEC nodes in the network and store their IDs in `mec_nodes`."""
        self.mec_nodes = [node_id for node_id, node in self.network.nodes.items() if node["type"] != "iot"]

    def _get_mec_links(self):
        """Identify MEC links (edges) in the network and store their IDs in `mec_links`."""
        self.mec_links = [link_id for link_id, link in self.network.edges.items() if link["type"] != "iot"]