import os, sys
import logging
import pandas as pd


# Get the logger specified in the file
logger = logging.getLogger(__name__)

class Collector:
    """Collect and manage simulation data logs.

    The `Collector` class is responsible for collecting various types of logs during a simulation run,
    including event logs, resource logs, SFC (Service Function Chain) logs, and runtime logs.
    It provides methods to log events, resources, SFCs, runtime information, and to generate reports.

    Attributes:
        event_logs (list): List to store event logs as dictionaries.
        resource_logs (list): List to store resource logs as dictionaries.
        sfc_logs (list): List to store SFC logs as dictionaries.
        runtime_logs (list): List to store runtime logs as dictionaries.

    """

    def __init__(self):
        self.event_logs = []
        self.resource_logs = []
        self.sfc_logs = []
        self.runtime_logs = []

    def log_event(self, event_log):
        """Log information of an event.

        Args:
            event_log (dict): A dictionary containing event log information.

        """
        self.event_logs.append(event_log)
        logger.debug(f"Collected event {event_log['event_id']}!")

    def log_resource(self, resource_log):
        """Log resource information of a resource instance sitting on a network node.

        Args:
            resource_log (dict): A dictionary containing resource log information.

        """
        self.resource_logs.append(resource_log)
        logger.debug(f"Collected resource {resource_log['network_id']}__{resource_log['resource_id']}!")

    def log_sfc(self, sfc_log):
        """Log SFC information of an SFC instance.

        Args:
            sfc_log (dict): A dictionary containing SFC log information.

        """
        self.sfc_logs.append(sfc_log)
        logger.debug(f"Collected sfc!")

    def log_runtime(self, timestamp, message, value, **kwargs):
        """Log runtime information.

        Args:
            timestamp (float): The timestamp of the runtime log entry.
            message (str): A message describing the runtime log entry.
            value (str, int, or float): The value associated with the runtime log entry.
            **kwargs: Additional keyword arguments for custom runtime log attributes.

        """
        rt_log = {
            "timestamp": timestamp,
            "message": message,
            "value": value
        }
        rt_log.update(kwargs)
        self.runtime_logs.append(rt_log)
        logger.debug(f"Collected Runtime!")

    def get_event_report(self):
        """Generate a Pandas DataFrame report for event logs.

        Returns:
            pd.DataFrame: A DataFrame containing event logs.

        """
        self.event_report = pd.DataFrame(self.event_logs)
        return self.event_report

    def get_resource_report(self):
        """Generate a Pandas DataFrame report for resource logs.

        Returns:
            pd.DataFrame: A DataFrame containing resource logs.

        """
        self.resource_report = pd.DataFrame(self.resource_logs)
        return self.resource_report

    def get_sfc_report(self):
        """Generate a Pandas DataFrame report for SFC logs.

        Returns:
            pd.DataFrame: A DataFrame containing SFC logs.

        """
        self.sfc_report = pd.DataFrame(self.sfc_logs)
        return self.sfc_report

    def get_runtime_report(self):
        """Generate a Pandas DataFrame report for runtime logs.

        Returns:
            pd.DataFrame: A DataFrame containing runtime logs.

        """
        self.runtime_report = pd.DataFrame(self.runtime_logs)
        return self.runtime_report

    def save_reports(self, directory=None):
        """Save generated reports to CSV files.

        Args:
            directory (str): The directory path to save the reports. If not provided, a "reports" directory
                will be created in the current working directory.

        """
        if not directory:
            directory = os.path.join(sys.path[0], "reports")
        os.makedirs(directory, exist_ok=True)
        for key, value in self.__dict__.items():
            if "report" in key:
                report_path = os.path.join(directory, key + ".csv")   
                value.to_csv(report_path)

    def load_report(self, filename):
        """Load a CSV report file into a Pandas DataFrame.

        Args:
            filename (str): The path to the CSV report file to be loaded.

        Returns:
            pd.DataFrame: A DataFrame containing the loaded report data.

        """
        return pd.read_csv(filename)