import simpy
import networkx as nx
import random
import logging
import numpy as np
import heapq
import json
import networkx as nx
from simulator.resource_allocator import ResourceAllocator
from simulator.events import BaseEvent as sim_event

# Get the logger specified in the file
logger = logging.getLogger(__name__)

class EventDispatcher:
    """An event dispatcher for managing service function chains (SFCs) in a simulation environment.

    This class is responsible for managing the deployment and scheduling of SFCs in a simulation environment. It uses
    a ResourceAllocator to allocate resources for SFCs and handles the creation of events for SFC nodes and links.

    Args:
        env (EnhancedEnvironment): The simulation environment.
        algorithm: The resource allocation algorithm.

    Attributes:
        env (EnhancedEnvironment): The simulation environment.
        network: The network topology used in the simulation.
        resource_allocator (ResourceAllocator): An instance of the ResourceAllocator class for resource allocation.
        deployed_sfcs (list): A list to store deployed SFCs.

    """

    def __init__(self, env, algorithm):
        """Initialize an EventDispatcher instance.

        Args:
            env (EnhancedEnvironment): The simulation environment.
            algorithm: The resource allocation algorithm.

        """
        self.env = env
        self.network = env.network
        self.resource_allocator = ResourceAllocator(env, algorithm)
        self.deployed_sfcs = []

    def request_action(self, sfc):
        """Request resource allocation for an SFC.

        Args:
            sfc: The SFC for which resource allocation is requested.

        Returns:
            SFC: The SFC with placed computing nodes.

        """
        return self.resource_allocator.get_allocation(sfc)

    def create_link(self, path, **kwargs):
        """Create link events for a given path in the network.

        Args:
            path (list): A list of nodes representing the path in the network.
            **kwargs: Additional keyword arguments for link creation.

        Returns:
            list: A list of link events.

        """
        network_path = []
        link_events = []
        # create a list with tuples (source, target) from the nodes in path
        for idx, source in enumerate(path[:-1]):
            target = path[(idx + 1) % len(path)]
            network_path.append((source, target))
        # append link events using the tuples from network_path to identify the link resources in the network
        for network_id in network_path:
            link_events.append(self.create_link_event(network_id, **kwargs))
        return link_events

    def create_node_event(self, sfc, sfc_node):
        """Create a network node event for a node in the SFC.

        Args:
            sfc: The SFC containing the node event.
            sfc_node: The ID of the node in the SFC.

        Returns:
            sim_event: An instance of the BaseEvent class representing the network node event.

        """
        node = sfc.graph.nodes[sfc_node]
        network_id = node["parameters"]["network_id"]
        resource_id = node["parameters"]["resource_id"]
        node["parameters"]["event_type"] = "task"
        node["parameters"]["env"] = self.env
        node["parameters"]["target_resource"] = self.network.nodes[network_id]["resources"][resource_id]
        node["parameters"]["sfc"] = sfc
        return sim_event(**node["parameters"])

    def create_link_event(self, network_id, sfc, sfc_link):
        """Create a network link event as part of a link description in the SFC.

        Args:
            network_id: The network identifier for the network link.
            sfc: The SFC containing the link.
            sfc_link: The link in the SFC containing the tranmission parameters.

        Returns:
            sim_event: An instance of the BaseEvent class representing the network link event.

        """
        link = sfc.graph.edges[sfc_link]
        resource_id = link["parameters"]["resource_id"]
        link["parameters"]["env"] = self.env
        link["parameters"]["event_id"] = str(sfc_link) + "_" + str(network_id)
        link["parameters"]["event_type"] = "link"
        link["parameters"]["network_id"] = network_id
        link["parameters"]["target_resource"] = self.network.edges[network_id]["resources"][resource_id]
        link["parameters"]["sfc"] = sfc
        event = sim_event(**link["parameters"])
        if self.network.edges[network_id]["type"] == "iot":
            event.request_timeout = 1e12
        return event

    def deploy_sfc(self, sfc):
        """Deploy an SFC in the simulation environment.

        This method deploys an SFC by scheduling its tasks and links in a topological order, but considering dependencies.
        The task graph of the SFC is represented as a DAG.

        Args:
            sfc: The SFC to be deployed.

        """
        sfc.start = self.env.now
        if not sfc.successful:
            sfc.end = self.env.now
            self.env.collector.log_runtime(self.env.now, "placement_failed", sfc.sfc_id)
            self.env.collector.log_sfc(sfc.log(self.env.now))
            return
        sfc.schedule = nx.DiGraph()
        sfc.placement_successful = True
        # schedule sfc nodes (=tasks) in topological order
        logger.info(f"T {np.around(self.env.now, 4)} - deploying SFC...")
        for event_id in nx.topological_sort(sfc.graph):
            node_event = self.create_node_event(sfc, event_id)
            sfc.schedule.add_node(event_id, event_obj=self.env.process(node_event.process()))
            # iterate through successors to determine the necessary links
            for successor_id in sfc.graph.successors(event_id):
                sfc_link = (event_id, successor_id)
                # read in the network path
                network_path = sfc.graph.edges[sfc_link]["parameters"]["network_path"]
                link_events = self.create_link(network_path, sfc=sfc, sfc_link=sfc_link)
                prev_event_id = event_id
                # schedule link events necessary to reach node of next task
                # add edges to include link events: (curr_sfc_node, link_event_1), (link_event_1, link_event_2), ..., (link_event_n, sucessor)
                for link_event in link_events:
                    curr_event_id = link_event.event_id
                    sfc.schedule.add_node(curr_event_id, event_obj=self.env.process(link_event.process()))
                    sfc.schedule.add_edge(prev_event_id, curr_event_id)
                    prev_event_id = link_event.event_id
                sfc.schedule.add_edge(prev_event_id, successor_id)
        logger.debug(f"T {np.around(self.env.now, 4)} - Task nodes {sfc.schedule.nodes}")
        logger.debug(f"T {np.around(self.env.now, 4)} - Task edges {sfc.schedule.edges}")
        # wait for all events to be completed before marking sfc as complete
        all_events = [event["event_obj"] for event in sfc.schedule.nodes.values()]
        yield simpy.AllOf(self.env, all_events)
        sfc.end = self.env.now
        self.env.collector.log_sfc(sfc.log(self.env.now))
        logger.info(f"T {np.around(self.env.now, 4)} - Finished SFC {sfc.sfc_id} with value {sfc.successful}")

    def dispatch(self):
        """Generate and deploy service function chains (SFCs) in the simulation environment.

        This method continuously generates and deploys SFCs in the simulation environment. It requests resource allocation
        for each generated SFC, deploys the SFC using the `deploy_sfc` method, and logs runtime information. The deployed
        SFCs are stored in the `deployed_sfcs` list.

        """
        logger.info(f"T {np.around(self.env.now, 4)} - Generating SFCs")
        while True:
            sfc = yield self.env.sfcs.get()
            self.env.collector.log_runtime(self.env.now, "request", 1)
            placed_sfc = self.request_action(sfc)
            deployed_sfc = self.env.process(self.deploy_sfc(placed_sfc)) 
            self.deployed_sfcs.append(deployed_sfc)
            