import numpy as np


class SFC:
    def __init__(self, sfc, **kwargs):
        """SFC (Service Function Chain) represents a service chain to be processed in a simulation.

        Args:
            sfc: The graph representing the service function chain.
            **kwargs: Additional keyword arguments.

        Attributes:
            graph: The graph representing the service function chain.
            schedule: The schedule for processing the SFC (not initialized by default).
            spawn_time (float): The spawn time of the SFC (initialized as positive infinity).
            max_delay (float): The maximum allowed delay for the SFC (initialized as positive infinity).
            delay (float): The execution delay of the SFC (initialized as positive infinity).
            start (float): The start time of processing the SFC (initialized as positive infinity).
            end (float): The end time of processing the SFC (initialized as positive infinity).
            successful (bool): Flag indicating the success of SFC processing (initialized as True).

        Methods:
            _update_state():
                Update the state of the SFC, i.e., execution delay and success.

            add_attribute(name, value):
                Add a new attribute to the SFC.

            log(timestamp):
                Capture relevant metrics of the SFC for logging in a dictionary.

            __lt__(other):
                Compare two SFC instances based on their spawn times.

        """
        self.graph = sfc
        self.schedule = None
        self.spawn_time = np.inf
        self.max_delay = np.inf
        self.delay = np.inf
        self.start = np.inf
        self.end = np.inf
        self.successful = True
        for key, value in kwargs.items():
            if key not in self.__dict__:
                setattr(self, key, value)

    def _update_state(self):
        """Update state of SFC, i.e., execution delay and success
        """
        self.delay = self.end - self.start
        if self.successful:
            self.successful = self.delay <= self.max_delay
    
    def add_attribute(self, name, value):
        """Add a new attribute to the SFC.

        Args:
            name (string): name of new attribute
            value (object): value of new attribute
        """
        setattr(self, name, value)

    def log(self, timestamp):
        """Capture relevant metrics of SFC for logging in a dictionary.

        Args:
            timestamp (float): current simulation time

        Returns:
            dict: collection of logs as dictionary
        """
        self._update_state()
        log_dict = {"timestamp": timestamp}
        log_dict.update(self.__dict__)
        keys_to_remove = ["graph", "schedule", "resource_model"]
        for key in keys_to_remove:
            del log_dict[key]
        return log_dict

    def __lt__(self, other):
        """Compare two SFC instances based on their spawn times."""
        return self.spawn_time < other.spawn_time