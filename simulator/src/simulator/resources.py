import simpy
import logging
import numpy as np

# Get the logger specified in the file
logger = logging.getLogger(__name__)

class BaseResource:

    def __init__(self, env, resource_host, network_id, resource_id, capacity, **kwargs):
        """BaseResource represents a resource object in the simulation environment. Simulation events can request
        to block a certain capacity of a resource object.

        Args:
            env (simpy.Environment): The simulation environment.
            resource_host: The network resource hosting the resource object.
            network_id: The identifier of the network to which the resource belongs.
            resource_id: The unique identifier of the resource object.
            capacity: The capacity of the resource.
            **kwargs: Additional keyword arguments.

        Attributes:
            env (simpy.Environment): The simulation environment.
            resource_host: The network resource hosting the resource object.
            network_id: The identifier of the network to which the resource belongs.
            resource_id: The unique identifier of the resource object.
            capacity: The capacity of the resource.
            resource (simpy.Container): The SimPy container representing the resource's capacity.
            users (list): List of users currently utilizing the resource.
            current_available_capacity (int): The current available capacity of the resource.
            current_available_capacity_rel (float): The relative available capacity (0 to 1) of the resource.
            current_occupation (int): The current occupation (in use) of the resource.
            current_occupation_rel (float): The relative occupation (0 to 1) of the resource.
            current_user_number (int): The current number of users utilizing the resource.
            current_queue_length (int): The current length of the resource's queue.
            failed_tasks (int): The number of failed tasks using the resource.
            total_tasks (int): The total number of tasks processed by the resource.
            blocking_probability (float): The blocking probability of the resource.
            last_timestamp (float): The timestamp of the last state update.

        Methods:
            _update_state(): Update the resource's state attributes.
            log(log_only=[]): Generate a log entry for the resource's current state.

        """

        self.env = env
        self.resource_host = resource_host
        self.network_id = network_id
        self.resource_id = resource_id
        self.capacity = capacity
        self.resource = simpy.Container(self.env, capacity=capacity, init=capacity)
        self.users = []

        self.current_available_capacity = int(self.resource.level)
        self.current_available_capacity_rel = float(self.current_available_capacity/self.capacity)
        self.current_occupation = int(self.capacity - self.resource.level)
        self.current_occupation_rel = float(self.current_occupation / self.capacity) 
        self.current_user_number = 0
        self.current_queue_length = 0
        self.failed_tasks = 0
        self.total_tasks = 0
        self.blocking_probability = 0
        self.last_timestamp = 0.0
        for key, value in kwargs.items():
            if key not in self.__dict__:
                setattr(self, key, value)

    def _update_state(self):
        """Update the resource's state attributes."""
        self.current_available_capacity = int(self.resource.level)
        self.current_available_capacity_rel = float(self.current_available_capacity/self.capacity)
        self.current_occupation = int(self.capacity - self.resource.level)
        self.current_occupation_rel = float(self.current_occupation / self.capacity)
        self.current_user_number = int(len(self.users))
        self.current_queue_length = int(len(self.resource.get_queue))
        self.failed_tasks = 0
        self.total_tasks = 0
        self.last_timestamp = self.env.now

    def log(self, log_only=[]):
        """Generate a log entry for the resource's current state.

        Args:
            log_only (list): A list of specific attributes to include in the log entry.

        Returns:
            dict or tuple: A dictionary representing the log entry or a tuple of two dictionaries
                when specific attributes are requested in log_only.

        """
        if log_only == []:
            log_dict = {"timestamp": self.env.now, "dt": self.env.now - self.last_timestamp}
            log_dict.update(self.__dict__)
            keys_to_remove = ["env", "resource", "users"]
            for key in keys_to_remove:
                del log_dict[key]
            self._update_state()
            return log_dict
        else:
            log_dict = {"timestamp": self.env.now, "dt": self.env.now - self.last_timestamp}
            log_dict.update(self.__dict__)
            keys_to_remove = ["env", "resource", "users"]
            for key in keys_to_remove:
                del log_dict[key]
            self._update_state()
            log_dict_now = {}
            for key in log_only:
                log_dict_now[key] = self.__dict__[key]
            return log_dict, log_dict_now


class NodeResource(BaseResource):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

class LinkResource(BaseResource):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.network_id = tuple(self.network_id)
