from random import Random

class RNG(Random):
    """RNG (Random Number Generator) is a class that extends the Python standard library's Random class
    to provide various random number generation methods.

    Args:
        seed (int): The seed value for initializing the random number generator.

    Attributes:
        Inherits attributes from the Random class.

    Methods:
        get(): Generate a random number based on the specified distribution.
    """ 
    def __init__(self, seed):
        super().__init__(seed)

    def get(self, distribution, **kwargs):
        """Generate a random number based on the specified distribution.

        Args:
            distribution (str): The type of random distribution to use. Supported values are:
                - "randint": Generate a random integer.
                - "absnormal": Generate a random absolute value following a normal distribution.
                - "expo": Generate a random number following an exponential distribution.
                - "uniform": Generate a random number following a uniform distribution.
            **kwargs: Additional keyword arguments specific to each distribution.

        Returns:
            float or int: The generated random number.

        Raises:
            NotImplementedError: If an unknown distribution is specified.
        """
        if distribution == "randint":
            return self.randint(**kwargs)
        elif distribution == "absnormal":
            return abs(self.normalvariate(**kwargs))
        elif distribution == "expo":
            return self.expovariate(**kwargs)
        elif distribution == "uniform":
            return self.uniform(**kwargs)
        else:
            raise NotImplementedError("Unknown distribution")