import networkx as nx
from abc import ABC, abstractmethod
import numpy as np
from simulator.rng import RNG

class BaseAlgorithm(ABC):
    """Base class for implementing algorithms that handle 

    This abstract base class provides a framework for implementing network algorithms
    used in a network simulation.

    Methods:
        setup_algorithm(config): Initialize algorithm-specific parameters.
        load_network(network_topo): Load network topology from a JSON representation.
        update_network_status(network_status_json): Update the network status from a JSON representation.
        place_sfc(sfc): Abstract method to be implemented by concrete algorithms.
        json2graph(json): Convert JSON data to a network graph.
        graph2json(G): Convert a network graph to JSON data.
        _generate_computation_parameters(event_id, network_id, **kwargs): Generate parameters of computation events in the network simulator.
        _generate_transmission_parameters(network_path, **kwargs): Generate parameters for transmission events.

    Attributes:
        config (dict): Algorithm-specific configuration parameters.
        rng (RNG): Random number generator.
        network (NetworkX Graph): Network topology.
        network_status (NetworkX Graph): Current network status.

    """

    def setup_algorithm(self, config):
        """Initialize algorithm-specific parameters.

        Args:
            config (dict): Algorithm-specific configuration parameters.

        """
        self.config = config
        self.rng = RNG(config["seed"])
        self.network = None
        self.network_status = None
        
    def load_network(self, network_topo):
        """Load network topology from a JSON representation.

        Args:
            network_topo (dict): JSON representation of network topology.

        """
        self.network = self.json2graph(network_topo)

    def update_network_status(self, network_status_json):
        """Update the network status attribute in the algorithm from a JSON representation.

        Args:
            network_status_json (dict): JSON representation of network status.

        """
        self.network_status = self.json2graph(network_status_json)

    def _update_network_status(self):
        """Update the network attribute in the algorithm by extracting the current metrics of the network_status attribute (internal method).
        """
        for node_id, node in self.network_status.nodes.items():
            self.network.nodes[node_id]["utilization"] = node["computational_resource"]["current_occupation_rel"]
            self.network.nodes[node_id]["available_capacity"] = node["computational_resource"]["current_available_capacity"]
            bp = node["computational_resource"]["failed_tasks"] / node["computational_resource"]["total_tasks"] if node["computational_resource"]["total_tasks"] > 0 else 0
            self.network.nodes[node_id]["bp"] = bp
        for link_id, link in self.network_status.edges.items():
            self.network.edges[link_id]["utilization"] = link["tunnel"]["current_occupation_rel"] 
            self.network.edges[link_id]["available_capacity"] = link["tunnel"]["current_available_capacity"]
            bp = link["tunnel"]["failed_tasks"] / link["tunnel"]["total_tasks"] if link["tunnel"]["total_tasks"] > 0 else 0
            self.network.edges[link_id]["bp"] = bp

    @abstractmethod
    def place_sfc(self, sfc):
        """Abstract method to place a Service Function Chain (SFC) in the network.

        Args:
            sfc (NetworkX DiGraph): Directed graph representing the SFC.
        """
        raise NotImplementedError("Please implement")

    def _generate_computation_parameters(self, event_id, network_id, **kwargs):
        """Generate parameters of computation event for use in the network simulator.

        Args:
            event_id (str): id of the aggregator task
            network_id (str): network node where aggregator should be placed

        Returns:
            dict: parameters
        """
        parameters = self._identify_sfc(**kwargs)
        parameters.update({
            "event_id": event_id,
            "task_type": "aggregation",
            "network_id": network_id,
            "resource_id": self.config["aggregation"]["resource_id"],
            "request_timeout": self.config["aggregation"]["request_timeout"],
            "requested_capacity": self.rng.get(**self.config["aggregation"]["requested_capacity"])
        })
        return parameters

    def _generate_transmission_parameters(self, network_path, **kwargs):
        """Generate link parameters for use in the network simulator

        Args:
            network_path (list): sequence of nodes to travel from the link source to the link target

        Returns:
            dict: parameters
        """
        parameters = self._identify_sfc(**kwargs)
        parameters.update({
            "resource_id": self.config["link"]["resource_id"],
            "network_path": network_path,
            "request_timeout": self.config["link"]["request_timeout"],
            "requested_capacity": self.rng.get(**self.config["link"]["requested_capacity"])
        })
        return parameters

    def _identify_sfc(self, G):
        node = list(G)[0]
        node_data = G.nodes[node]
        return {
            "application_type": node_data["parameters"]["application_type"],
            "sfc_id": node_data["parameters"]["sfc_id"]
        }

    def json2graph(self, json):
        """Convert JSON data to a NetworkX graph.

        Args:
            json (dict): JSON representation of a directed graph.

        Returns:
            NetworkX DiGraph: Directed graph constructed from JSON data.

        """
        return nx.node_link_graph(json, directed=True, multigraph=False)

    def graph2json(self, G):
        """Convert a NetworkX graph to JSON data.

        Args:
            G (NetworkX DiGraph): Directed graph to be converted.

        Returns:
            dict: JSON representation of the graph.

        """
        return nx.node_link_data(G)