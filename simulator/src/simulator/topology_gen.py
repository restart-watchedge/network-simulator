import networkx as nx
import json
from simulator.resources import NodeResource, LinkResource
import logging
from simulator.rng import RNG

class TopologyGenerator():
    """TopologyGenerator generates network topologies for the simulator.

    Args:
        seed (int, optional): The seed value for the random number generator. Defaults to 10.

    Attributes:
        rng (RNG): The random number generator used for creating topologies.
        mec_network (networkx.Graph): The Mobile Edge Computing (MEC) network topology.
        config (dict): The configuration settings for the network topology.

    Methods:
        set_config(config):
            Set the configuration settings for the network topology.

        read_mec_topo(filename, file_type="edge list"):
            Read a MEC network topology from a file.

        create_topo(random_end=False):
            Create a network topology, including end nodes.

        save_topo(filename):
            Save the generated network topology to a file.

    """
    def __init__(self, seed=10):
        self.rng = RNG(seed)
        self.mec_network = nx.erdos_renyi_graph(10, 0.5, seed=10, directed=True)
        self.config = {
            "device_amount": 10,
            "nodes": {
                "iot": {
                    "capacity": 1
                },
                "edge": {
                    "capacity": 20,
                    "f": 3e9
                },
                "cloud": {
                    "capacity": 2000,
                    "f": 3.4e9
                }
            },
            "links": {
                "iot": {
                    "capacity": 200
                },
                "edge": {
                    "capacity": 1000
                },
                "cloud": {
                    "capacity": 1000
                },
            }
        }

    def set_config(self, config):
        """Set the configuration settings for the network topology.

        Args:
            config (dict): The new configuration settings.
        """
        self.config = config
    
    def read_mec_topo(self, filename, file_type="edge list"):
        """Read a MEC network topology from a file.

        Args:
            filename (str): The path to the file containing the network topology.
            file_type (str, optional): The type of file to read (e.g., "edge list" or "json"). Defaults to "edge list".
        """
        if file_type == "edge list":
            self.mec_network = nx.read_edgelist(filename, create_using=nx.DiGraph, data=False)
        elif file_type == "json":
            self.mec_network = self._read_network_from_json(filename)
        else:
            raise NotImplementedError("Filetype not implemented!")
    
    def _read_network_from_json(self, filename):
        """Read a network topology from a JSON file.

        This function reads network topology data from a JSON file and constructs a networkx graph
        with directed edges (DiGraph) without multigraph support.

        Args:
            filename (str): The path to the JSON file containing network topology data.

        Returns:
            networkx.DiGraph: A directed graph representing the network topology.

        Note:
            The JSON file should be formatted according to the node-link format compatible with
            `networkx.node_link_graph`. The resulting graph should represent the network topology.

        """
        with open(filename) as json_file:
            network_data = json.load(json_file)
        return nx.node_link_graph(network_data, directed=True, multigraph=False)

    def create_topo(self, random_end=False):
        """Create a network topology, including end nodes.

        Args:
            random_end (bool, optional): Whether to add a random number of end nodes per mec node. Defaults to False.
        """
        self._create_mec()
        self._add_end_nodes(random_end)
        # for node_id, node in self.topology.nodes.items():
        #     print(f"{node_id}: {node}")

    def _create_mec(self):
        """Create a Mobile Edge Computing (MEC) network topology based on the current configuration.

        This function generates an MEC network topology and relabels its nodes based on their type (cloud or edge). The
        cloud node is randomly chosen among the MEC nodes. 
        It also updates the resource specifications for nodes and links in the topology.

        Note:
            This function is intended for internal use within the `TopologyGenerator` class.
        """
        cloud_nodes = self.rng.choices(list(self.mec_network.nodes), k=1)
        edge_counter = 1
        cloud_counter = 1
        node_mapping = {}
        for node_id, node in self.mec_network.nodes.items():
            if node_id not in cloud_nodes:
                node_mapping[node_id] = "edge_" + str(edge_counter) + "_1"
                edge_counter += 1
            else:
                node_mapping[node_id] = "cloud_" + str(cloud_counter) + "_1"
                cloud_counter += 1
        
        self.topology = nx.relabel_nodes(self.mec_network, node_mapping)

        for node_id in self.topology.nodes.keys():
            if  "edge" in node_id:
                self.topology.nodes[node_id].update(self._add_node_parameters("edge"))
                for link_id in self.topology.out_edges(node_id):
                        self.topology.edges[link_id].update(self._add_link_parameters("edge"))
            else:
                self.topology.nodes[node_id].update(self._add_node_parameters("cloud"))
                for link_id in self.topology.out_edges(node_id):
                    self.topology.edges[link_id].update(self._add_link_parameters("cloud"))
                for link_id in self.topology.in_edges(node_id):
                    self.topology.edges[link_id].update(self._add_link_parameters("cloud"))
    
    def _add_end_nodes(self, random_end=False):
        """Add end nodes (also IoT devices or clients) to the topology.

        Args:
            random_end (bool, optional): Whether to add a random number of end nodes per mec node. Defaults to False.

        """
        end_nodes = nx.DiGraph()
        for node_id, node in self.topology.nodes.items():
            if node["type"] == "edge":
                site = node_id.split("_")[1]
                device_counter = 1
                if random_end:
                    device_amount = self.rng.randint(self.config["device_amount"]/2, self.config["device_amount"])
                else: 
                    device_amount = self.config["device_amount"]
                for count in range(device_amount):
                    device_id = "iot_" + site + "_" + str(device_counter)
                    end_nodes.add_node(device_id, **self._add_node_parameters("iot"))
                    end_nodes.add_edge(device_id, node_id, **self._add_link_parameters("iot"))
                    device_counter += 1
        self.topology.update(end_nodes)

    def _add_node_parameters(self, node_type):
        """Add node parameters to the resource specifications based on node type.

        Args:
            node_type (str): The type of the node (e.g., "edge" or "cloud").

        Returns:
            dict: Node parameters to be added to the resource specifications.

        """
        node_config = self.config["nodes"]
        params = {
            "type": node_type,
            "resource_specs": {
                "computational_resource": {
                    **node_config[node_type],
                    "type": node_type
                }
            }
        }
        return params

    def _add_link_parameters(self, link_type):
        """Add link parameters to the resource specifications based on link type.

        Args:
            link_type (str): The type of the link (e.g., "iot", "edge", or "cloud").

        Returns:
            dict: Link parameters to be added to the resource specifications.

        """
        link_config = self.config["links"]
        params = {
            "type": link_type,
            "resource_specs": {
                "tunnel": {
                    **link_config[link_type],
                    "type": link_type
                }
            }
        }
        return params

    def save_topo(self, filename):
        """Save the generated network topology to a file in JSON format.

        Args:
            filename (str): The path to the output file.

        """
        with open(filename, 'w+') as outfile:
            json.dump(nx.node_link_data(self.topology), outfile, indent = 4,)
