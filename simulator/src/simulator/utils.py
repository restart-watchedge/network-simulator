"""Useful functions
"""
import json

def read_json(json_file_path):
    """Read JSON data from a file.

    This function reads JSON data from a specified file and returns it as a Python dictionary.

    Args:
        json_file_path (str): The path to the JSON file to be read.

    Returns:
        dict: A Python dictionary containing the parsed JSON data.
    """
    with open(json_file_path) as json_file:
        data = json.load(json_file)
    return data