from abc import ABC, abstractmethod

class BaseResourceModel(ABC):
    """This class provides the API for resource models to be included in the simulation.

    Parameters relevant for the resource models are directly passed by the simulator events
    to the resource model instance.

    Attributes:
        env (simpy.Environment): The simulation environment in which the resource model operates.

    """
    def __init__(self, env):
        self.env = env

    @abstractmethod
    def calculate_delay(self):
        """Abstract method to calculate delay in the resource model.

        This method should be implemented in derived classes to calculate the delay
        of a simulation event specific to the resource model.

        Returns:
            float: The calculated delay value.

        Raises:
            NotImplementedError: When this method is called on the base class.
        
        """
        raise NotImplementedError("Please implement")
