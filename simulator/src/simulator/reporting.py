import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
from matplotlib.ticker import MaxNLocator
import pandas as pd
import networkx as nx
import math
from itertools import product
from simulator.counter import TimeIndependentCounter
from simulator.collector import Collector


def read_reports(folder, topos, algs, runs, parameter_values=None):
    """
    Read simulation reports from files and return metadata and report data.

    Args:
        folder (str): The folder where reports are located.
        topos (list): A list of topology names.
        algs (list): A list of algorithm names.
        runs (int): The number of simulation runs.
        parameter_values (list, optional): A list of parameter values (default None).

    Returns:
        pd.DataFrame: Metadata containing the combination of parameters.
        list: A list of dictionaries containing simulation reports as pd.DataFrame.
    """
    meta = []
    reports = []
    c = Collector()
    i = 0
    if parameter_values:
        combinations = product(topos, algs, parameter_values)
    else:
        combinations = product(topos, algs)
    for combination in combinations:
        if parameter_values:
            directory = folder + combination[0] + "_" + combination[1] + "__" + str(runs) + "requests_" + str(combination[2]) + "parameter/"
            m = {"topo": combination[0], "algorithm": combination[1], "parameter": combination[2], "i": i}
        else:
            directory = folder + combination[0] + "_" + combination[1] + "__" + str(runs) + "requests/"
            m = {"topo": combination[0], "algorithm": combination[1], "i": i}
        entry = {}
        entry["sfc_report"] = c.load_report(directory + "sfc_report.csv")
        entry["res_report"] = c.load_report(directory + "resource_report.csv")
        entry["rt_report"] = c.load_report(directory + "runtime_report.csv")
        meta.append(m)
        reports.append(entry)
        i += 1
    return pd.DataFrame(meta), reports

class ReportResources:
    """
    Class for analyzing and visualizing resource-related simulation reports.
    """
    
    def get_metrics(self, res_report, **kwargs):
        """
        Compute resource-related metrics from a resource report.

        Args:
            res_report (pd.DataFrame): The resource report.
            **kwargs: Additional keyword arguments.

        Returns:
            dict: A dictionary of resource metrics.
        """
        edge_nodes, cloud_nodes, edge_links, cloud_links = self.get_resource_groups(res_report)

        # Sum of failed tasks per interval/total tasks per interval
        mean_bp_resources = np.sum(res_report["failed_tasks"]) / np.sum(res_report["total_tasks"])

        # shown it works mathematically bc denominator always the same!
        mean_ut_edge_nodes = self.get_time_weighted(edge_nodes["current_occupation_rel"], edge_nodes["dt"], np.sum(edge_nodes["dt"])).sum()
        mean_ut_edge_links = self.get_time_weighted(edge_links["current_occupation_rel"], edge_links["dt"], np.sum(edge_links["dt"])).sum()
        mean_ut_cloud_nodes = self.get_time_weighted(cloud_nodes["current_occupation_rel"], cloud_nodes["dt"], np.sum(cloud_nodes["dt"])).sum()
        mean_ut_cloud_links = self.get_time_weighted(cloud_links["current_occupation_rel"], cloud_links["dt"], np.sum(cloud_links["dt"])).sum()
    
        
        return {
            "mean_bp_resources": mean_bp_resources,
            "mean_ut_edge_nodes": mean_ut_edge_nodes, 
            "mean_ut_edge_links": mean_ut_edge_links, 
            "mean_ut_cloud_nodes": mean_ut_cloud_nodes, 
            "mean_ut_cloud_links": mean_ut_cloud_links
        }

    def get_time_weighted(self, values, dt, t_max):
        """
        Calculate the time-weighted values based on input values, time intervals, and the total time.

        Args:
            values (array-like): Input values.
            dt (array-like): Time intervals.
            t_max (float): Total time.

        Returns:
            array-like: Time-weighted values.
        """
        return np.array(values) * np.array(dt) / t_max
    
    def edge_cloud_hist(self, res_report, metric, xlabel, figsize=(14,10)):
        """
        Generate a histogram of resource metrics for edge and cloud resources.

        Args:
            res_report (pd.DataFrame): The resource report.
            metric (str): The resource metric to visualize.
            xlabel (str): The label for the x-axis.
            figsize (tuple, optional): The figure size (default (14, 10)).

        Returns:
            Figure: The generated matplotlib figure.
        """
        edge_nodes, cloud_nodes, edge_links, cloud_links = self.get_resource_groups(res_report)
        
        ylabel = "time share"
        fig, axs = plt.subplots(2,2, figsize=figsize)
        self._hist(edge_nodes[metric], axs[0,0], "edge nodes", xlabel, ylabel, weights=edge_nodes["dt"] / np.sum(edge_nodes["dt"]))
        self._hist(cloud_nodes[metric], axs[0,1], "cloud nodes", xlabel, ylabel, weights=cloud_nodes["dt"] / np.sum(cloud_nodes["dt"]))
        self._hist(edge_links[metric], axs[1,0], "edge links", xlabel, ylabel, weights=edge_links["dt"] / np.sum(edge_links["dt"]))
        self._hist(cloud_links[metric], axs[1,1], "cloud links", xlabel, ylabel, weights=cloud_links["dt"] / np.sum(cloud_links["dt"]))
        
        return fig

    def edge_cloud_time(self, res_report, metric, freq, xlabel, y_label, figsize=(14,10)):
        """
        Generate a time-series plot of resource metrics for edge and cloud resources.

        Args:
            res_report (pd.DataFrame): The resource report.
            metric (str): The resource metric to visualize.
            freq (str): The frequency of data points (e.g., "1S").
            xlabel (str): The label for the x-axis.
            y_label (str): The label for the y-axis.
            figsize (tuple, optional): The figure size (default (14, 10)).

        Returns:
            Figure: The generated matplotlib figure.
        """
        edge_nodes, cloud_nodes, edge_links, cloud_links = self.get_resource_groups(res_report)
        
        ylabel = "ratio"
        fig, axs = plt.subplots(2,2, figsize=figsize)

        self.plot_metric_over_time(axs[0,0], edge_nodes, metric, freq=freq, label="edge nodes")
        self.plot_metric_over_time(axs[0,1], cloud_nodes, metric, freq=freq, label="cloud nodes")
        self.plot_metric_over_time(axs[1,0], edge_links, metric, freq=freq, label="edge links")
        self.plot_metric_over_time(axs[1,1], cloud_links, metric, freq=freq, label="cloud links")

        return fig

    def get_resource_groups(self, res_report):
        """
        Split the resource report into different resource groups.

        Args:
            res_report (pd.DataFrame): The resource report.

        Returns:
            tuple: Edge nodes, cloud nodes, edge links, and cloud links DataFrames.
        """
        nodes, links = self.split_nodes_links(res_report)

        edge_nodes = nodes[nodes["type"] == "edge"]
        cloud_nodes = nodes[nodes["type"] == "cloud"]
        # links between edge nodes
        edge_links = links[links["type"] == "edge"]
        # links directed to cloud node
        cloud_links = links[links["type"] == "cloud"]

        return edge_nodes, cloud_nodes, edge_links, cloud_links
        
    def _hist(self, data, ax, label, xlabel, ylabel, weights=[None]):
        if not isinstance(weights, pd.Series):
            weights = np.ones(len(data)) * 1.0/len(data)
        if any(data>1.0):
            bins = math.ceil(math.sqrt(len(data)))
        else:
            bins = np.arange(0,1.01,0.01)
        ax.hist(data, bins, weights=weights, label=label)
        ax.set_xlabel(xlabel)
        ax.set_ylabel(ylabel)
        ax.legend()
        ax.tick_params(direction="in")
        
    def split_nodes_links(self, df):
        """
        Split a DataFrame into two DataFrames: one for nodes and one for links.

        Args:
            df (pd.DataFrame): The input DataFrame.

        Returns:
            tuple: DataFrames containing nodes and links.
        """
        nodes = df[df["resource_host"] == "node"]
        links = df[df["resource_host"] == "link"]
        return nodes, links

    def plot_resource_metric_over_time(self, ax, res_report, resource, metric="current_occupation_rel", freq="1S", **kwargs):
        """
        Plot a resource metric over time for a specific resource.

        Args:
            ax (matplotlib.axes._axes.Axes): The matplotlib axes to plot on.
            res_report (pd.DataFrame): The resource report.
            resource (str): The name of the resource to plot.
            metric (str, optional): The resource metric to visualize (default "current_occupation_rel").
            freq (str, optional): The frequency of data points (e.g., "1S").
            **kwargs: further parameters if needed for referenced methods.
        """
        resource_inspected = res_report[res_report["network_id"] == resource].copy()
        aggregated_metric = self.metric_over_time(resource_inspected, metric, freq)
        self.series_line_plot(ax, aggregated_metric, metric, **kwargs)
        self.set_ax_param(ax, "ratio", **kwargs)

    def plot_metric_over_time(self, ax, report, metric="current_occupation_rel", freq="1S", **kwargs):
        """
        Plot a metric over time for a given resource report.

        Args:
            ax (matplotlib.axes._axes.Axes): The matplotlib axes to plot on.
            report (pd.DataFrame): The report containing resource metrics over time.
            metric (str, optional): The resource metric to visualize (default "current_occupation_rel").
            freq (str, optional): The frequency of data points (e.g., "1S").
            **kwargs: Additional keyword arguments.
        """
        report_copy = report.copy()
        aggregated_metric = self.metric_over_time(report_copy, metric, freq)
        self.series_line_plot(ax, aggregated_metric, metric, **kwargs)
        self.set_ax_param(ax, "ratio", **kwargs)

    def series_line_plot(self, ax, series, metric, marker=".", label=None, **kwargs):
        """
        Create a line plot for a time series.

        Args:
            ax (matplotlib.axes._axes.Axes): The matplotlib axes to plot on.
            series (pd.Series): The time series data to plot.
            metric (str): The metric being visualized.
            marker (str, optional): The marker style for the data points (default ".").
            label (str, optional): The label for the plot (default None).
            **kwargs: Additional keyword arguments.

        Returns:
            None
        """
        if label:
            ax.plot(series.index, series, marker=marker, label=label, **kwargs)
        else:
            labels = {
                "current_occupation_rel": "relative occupation",
                "blocking_probability": "blocking probability"
            }
            ax.plot(series.index, series, marker=marker, label=labels[metric], **kwargs)

    def set_ax_param(self, ax, ylabel, xlabel="simulation time / s", marker=".", labelrotation=0, **kwargs):
        """
        Set parameters for the matplotlib axes.

        Args:
            ax (matplotlib.axes._axes.Axes): The matplotlib axes to configure.
            ylabel (str): The label for the y-axis.
            xlabel (str, optional): The label for the x-axis (default "simulation time / s").
            marker (str, optional): The marker style for data points (default ".").
            labelrotation (int, optional): The rotation angle for x-axis labels (default 0).
            **kwargs: Additional keyword arguments.
        """
        ax.set_xlabel(xlabel)
        ax.set_ylabel(ylabel)
        ax.tick_params(axis="x", labelrotation=labelrotation)
        ax.legend()
    
    def metric_over_time(self, df, metric, freq):
        """
        Calculate the aggregated metric over time for a given DataFrame.

        Args:
            df (pd.DataFrame): The DataFrame containing metric data.
            metric (str): The metric to compute.
            freq (str): The frequency of data points (e.g., "1S").

        Returns:
            pd.Series: The aggregated metric over time.
        """
        if metric == "blocking_probability":
            agg_metric = self._bp_over_time(df, freq)
        else:
            df["weighted_" + metric] = self.get_time_weighted(df[metric], df["dt"], 1)
            weighted_metric = self._agg_sum(df, "timestamp", "weighted_" + metric, freq=freq)
            weight_per_interval = self._agg_sum(df, "timestamp", "dt", freq=freq)
            agg_metric = weighted_metric / weight_per_interval
            agg_metric.fillna(0, inplace=True)
        return agg_metric

    def _bp_over_time(self, df, freq):
        """
        Calculate the blocking probability over time for a given DataFrame. Exists due to different calculation from metric_over_time()

        Args:
            df (pd.DataFrame): The DataFrame containing blocking probability data.
            freq (str): The frequency of data points (e.g., "1S").

        Returns:
            pd.Series: The blocking probability over time.
        """
        failed_tasks = self._agg_sum(df, "timestamp", "failed_tasks", freq=freq)
        total_tasks = self._agg_sum(df, "timestamp", "total_tasks", freq=freq)
        resource_bp_over_time = failed_tasks / total_tasks
        resource_bp_over_time.fillna(0, inplace=True)
        return resource_bp_over_time

    def _agg_sum(self, df, index, column, freq="1S"):
        """
        Resample and sum a DataFrame based on a specific column.

        Args:
            df (pd.DataFrame): The DataFrame to resample and sum.
            index (str): The index column to use for resampling.
            column (str): The column to sum.
            freq (str): The frequency of data points (e.g., "1S").

        Returns:
            pd.Series: The resampled and summed data series.
        """
        resampled_df = df.set_index(df[index])
        resampled_df.index = pd.to_datetime(resampled_df.index, unit="ms")
        resampled_df = resampled_df[column].resample(freq).sum()
        resampled_df.index = resampled_df.index.view(int) // 10 ** 9
        return resampled_df
    
    def draw_metric(self, ax, df, metric="current_occupation_rel", pos=None, cmap="jet", node_size=800, arrowsize=10, edge_width=2, font_size=14, font_color='black', horz=0.0, vert=0.05, vmin=None, vmax=None, alpha=(0.1,1), labels={"cloud_1_1": "cloud"}):
        """
        Draw a network graph with nodes and edges colored by a specific metric.

        To use it:
        fig, (ax1, ax2) = plt.subplots(ncols=2)
        sm = draw_metric(ax1)
        ax_cb = fig.add_axes([0.95, 0.2, 0.02, 0.6])  # [left, bottom, width, height]
        fig.colorbar(sm, cax=ax_cb)
        plt.show()

        Args:
            ax (matplotlib.axes._axes.Axes): The matplotlib axes to plot on.
            df (pd.DataFrame): The resource report DataFrame.
            metric (str, optional): The metric to use for coloring (default "current_occupation_rel").
            pos (dict, optional): Node positions (default None).
            cmap (str, optional): Colormap to use for coloring (default "jet").
            node_size (int, optional): Node size (default 800).
            arrowsize (int, optional): Size of arrows for edges (default 10).
            edge_width (int, optional): Width of edges (default 2).
            font_size (int, optional): Font size for node labels (default 14).
            font_color (str, optional): Font color for node labels (default 'black').
            horz (float, optional): Horizontal offset for node labels (default 0.0).
            vert (float, optional): Vertical offset for node labels (default 0.05).
            vmin (float, optional): Minimum value for color normalization (default None).
            vmax (float, optional): Maximum value for color normalization (default None).
            alpha (tuple, optional): Range for alpha values to define opacity of nodes and edges (default (0.1, 1)).
            labels (dict, optional): Node labels (default {"cloud_1_1": "cloud"}).

        Returns:
            ScalarMappable: ScalarMappable for color normalization.
        """
        nodes, links = self.split_nodes_links(df)
        # add metrics for graph nodes and edges to a dict compatible with drawing function
        if metric == "blocking_probability":
            node_metrics = {k:{metric: v} for k, v in self.bp_per_resource(nodes).items()}
            link_metrics = {k:{metric: v} for k, v in self.bp_per_resource(links).items()}
        else:
            per_nodes = self.logs_per_resource(nodes)
            per_links = self.logs_per_resource(links)
            node_metrics = {curr["network_id"].iloc[0]: {metric: self.get_time_weighted(curr[metric], curr["dt"], np.sum(curr["dt"])).sum()} for curr in per_nodes}
            link_metrics = {curr["network_id"].iloc[0]: {metric: self.get_time_weighted(curr[metric], curr["dt"], np.sum(curr["dt"])).sum()} for curr in per_links}

        G = nx.DiGraph()
        for nid, node in node_metrics.items():
            G.add_node(nid, **node)
        for lid, link in link_metrics.items():
            G.add_edge(*eval(lid), **link)
        # # Define node and edge colors based on some metric
        node_color = []
        for node in G.nodes.values():
            if node != {}:
                node_color.append(node[metric])
            else:
                node_color.append(0.0)
        edge_color = [G[u][v][metric] for u, v in G.edges()]
        edge_alpha = np.interp(edge_color, [min(edge_color), max(edge_color)], [alpha[0], alpha[1]])

        if not vmin:
            vmin = min([min(node_color), min(edge_color)])
        if not vmax:
            vmax = min([max(node_color), max(edge_color)])

        # Draw the graph
        cmap = plt.cm.get_cmap(cmap)
        norm = plt.Normalize(vmin=vmin, vmax=vmax)
        sm = plt.cm.ScalarMappable(cmap=cmap, norm=norm)
        sm.set_array([])
        if not pos:
            pos = nx.spring_layout(G)  # Choose a layout for the nodes
        nx.draw_networkx_nodes(G, pos, node_color=node_color, cmap=cmap, node_size=node_size, ax=ax, vmin=vmin, vmax=vmax)
        nx.draw_networkx_edges(G, pos, edge_color=edge_color, edge_cmap=cmap, width=edge_width, alpha=edge_alpha, ax=ax, edge_vmin=vmin, edge_vmax=vmax, node_size=node_size, arrowsize=arrowsize)
        nx.draw_networkx_labels(G, pos={k: (v[0] + horz, v[1] + vert) for k, v in pos.items()}, labels=labels, font_size=font_size, font_color=font_color, ax=ax)

        return sm
    
    def draw_graph(self, ax, df, pos=None, cmap="jet", node_size=800, arrowsize=10, edge_width=2, font_size=14, font_color='black', horz=0.0, vert=0.05):
        """
        Draw a network graph with nodes and edges based on resource types.

        To use it:
        fig, (ax1, ax2) = plt.subplots(ncols=2)
        sm = draw_graph(ax1)
        ax_cb = fig.add_axes([0.95, 0.2, 0.02, 0.6])  # [left, bottom, width, height]
        fig.colorbar(sm, cax=ax_cb)
        plt.show()

        Args:
            ax (matplotlib.axes._axes.Axes): The matplotlib axes to plot on.
            df (pd.DataFrame): The resource report DataFrame.
            pos (dict, optional): Node positions (default None).
            cmap (str, optional): Colormap to use for coloring (default "jet").
            node_size (int, optional): Node size (default 800).
            arrowsize (int, optional): Size of arrows for edges (default 10).
            edge_width (int, optional): Width of edges (default 2).
            font_size (int, optional): Font size for node labels (default 14).
            font_color (str, optional): Font color for node labels (default 'black').
            horz (float, optional): Horizontal offset for node labels (default 0.0).
            vert (float, optional): Vertical offset for node labels (default 0.05).

        Returns:
            dict: Node positions.
        """
        edge_nodes, cloud_nodes, edge_links, cloud_links = self.get_resource_groups(df) 
        edge_node_list = list(edge_nodes["network_id"].unique())
        cloud_nodes_list = list(cloud_nodes["network_id"].unique())
        edge_links_list = list(edge_links["network_id"].unique())
        cloud_links_list = list(cloud_links["network_id"].unique())

        G = nx.DiGraph()
        for nid in edge_node_list:
            G.add_node(nid, type="edge")
        for nid in cloud_nodes_list:
            G.add_node(nid, type="cloud")
        for lid in edge_links_list:
            G.add_edge(*eval(lid), type="edge")
        for lid in cloud_links_list:
            G.add_edge(*eval(lid), type="cloud")
        # # Define node and edge colors based on some metric
        node_color = []
        edge_color = []
        for node in G.nodes.values():
            if node["type"] == "edge":
                node_color.append("#4B67DB")
            elif node["type"] == "cloud":
                 node_color.append("#F57514")
        for edge in G.edges.values():
            if edge["type"] == "edge":
                edge_color.append("#4B67DB")
            elif edge["type"] == "cloud":
                 edge_color.append("#F57514")

        # Draw the graph
        if not pos:
            pos = nx.spring_layout(G)  # Choose a layout for the nodes
        nx.draw_networkx_nodes(G, pos, node_color=node_color, node_size=node_size, ax=ax)
        nx.draw_networkx_edges(G, pos, edge_color=edge_color, width=edge_width, ax=ax, node_size=node_size, arrowsize=arrowsize)
        nx.draw_networkx_labels(G, pos={k: (v[0] + horz, v[1] + vert) for k, v in pos.items()}, font_size=font_size, font_color=font_color, ax=ax)
        return pos

    def logs_per_resource(self, df):
        """
        Split the resource report into logs per resource.

        Args:
            df (pd.DataFrame): The resource report DataFrame.

        Returns:
            list: List of DataFrames containing logs per resource.
        """
        network_id_list = df.network_id.unique()
        per_resource = []
        for n_id in network_id_list:
            curr_id = df[df["network_id"] == n_id]
            per_resource.append(curr_id)
        return per_resource
    
    def bp_per_resource(self, df):
        """
        Calculate the blocking probability per resource.

        Args:
            df (pd.DataFrame): The resource report DataFrame.

        Returns:
            dict: Dictionary of blocking probabilities per resource.
        """
        network_id_list = df.network_id.unique()
        per_resource = {}
        for n_id in network_id_list:
            curr_id = df[df["network_id"] == n_id]
            if np.sum(curr_id["total_tasks"]) > 0:
                bp_nid = float(np.sum(curr_id["failed_tasks"]) / np.sum(curr_id["total_tasks"]))
            else:
                bp_nid = 0.0
            per_resource[n_id] = bp_nid
        return per_resource

class ReportSFC:
    """
    Class for analyzing and visualizing SFC-related simulation reports.
    """

    def get_mean_placement_time(self, rt_report):
        """
        Get the mean placement time from a runtime report.

        Args:
            rt_report (pd.DataFrame): The runtime report.

        Returns:
            float: The mean placement time.
        """
        return rt_report[rt_report["message"] == "placement_time"]["value"].mean()

    def get_blocking_probability(self, sfc_report):
        """
        Calculate the blocking probability from an SFC report.

        Args:
            sfc_report (pd.DataFrame): The SFC report.

        Returns:
            float: The blocking probability.
        """
        successful_request_rate = np.sum(sfc_report["successful"]) / len(sfc_report)
        return 1 - successful_request_rate
    
    def get_metrics(self, sfc_report):
        """
        Calculate various SFC-related metrics from an SFC report.

        Args:
            sfc_report (pd.DataFrame): The SFC report.

        Returns:
            dict: A dictionary of SFC metrics.
        """
        successful_request_rate = np.sum(sfc_report["successful"]) / len(sfc_report)
        failed_request_rate = 1 - successful_request_rate
        mean_successful_request_duration = sfc_report[sfc_report["successful"]==True]["delay"].mean()

        return {
            "failed_request_rate": failed_request_rate,
            "mean_successful_request_duration": mean_successful_request_duration,
        }

    def sfc_stats(self, rt_report, sfc_report, figsize=(10, 12), freq="1S", labelrotation=0):
        """
        Generate a summary plot of SFC-related statistics. Includes a comparison of incoming and active SFCs
        and the blocking probability over time.

        Args:
            rt_report (pd.DataFrame): The runtime report.
            sfc_report (pd.DataFrame): The SFC report.
            figsize (tuple, optional): The figure size (default (10, 12)).
            freq (str, optional): The frequency of data points (e.g., "1S").
            labelrotation (int, optional): The rotation angle for x-axis labels (default 0).

        Returns:
            Figure: The generated matplotlib figure.
        """
        fig, axs = plt.subplots(2,1, figsize=figsize, sharex=True)

        # plot number of requests and active requests
        self.plot_incoming_active(axs[0], rt_report, freq=freq, labelrotation=labelrotation)
        
        # plot blocking probability
        self.plot_blocking_probability_bar(axs[1], sfc_report, freq=freq, labelrotation=labelrotation)
        
        return fig
    
    def plot_incoming_active(self, ax, rt_report, freq="1S", labelrotation=0):
        """
        Plot the number of incoming and active requests over time.

        Args:
            ax (matplotlib.axes._axes.Axes): The matplotlib axes to plot on.
            rt_report (pd.DataFrame): The runtime report.
            freq (str, optional): The frequency of data points (e.g., "1S").
            labelrotation (int, optional): The rotation angle for x-axis labels (default 0).
        """
        ax.yaxis.set_major_locator(MaxNLocator(integer=True))
        incoming = self.incoming_requests(rt_report, freq)
        incoming = pd.DataFrame(incoming)
        incoming.rename(columns={"timestamp": "incoming"}, inplace=True)
        incoming["active"] = self.active_requests(rt_report, freq)
        incoming.plot.bar(ax=ax)
        ax.set_xlabel("simulation time / s")
        ax.set_ylabel(f"requests")
        ax.tick_params(axis="x", labelrotation=labelrotation)
    
    def plot_incoming_requests(self, ax, rt_report, freq="1S", labelrotation=0):
        """
        Plot the number of incoming requests over time.

        Args:
            ax (matplotlib.axes._axes.Axes): The matplotlib axes to plot on.
            rt_report (pd.DataFrame): The runtime report.
            freq (str, optional): The frequency of data points (e.g., "1S").
            labelrotation (int, optional): The rotation angle for x-axis labels (default 0).
        """
        ax.yaxis.set_major_locator(MaxNLocator(integer=True))
        request_ps_count = self.incoming_requests(rt_report, freq)
        request_ps_count.plot.bar(ax=ax)
        ax.set_xlabel("simulation time / s")
        ax.set_ylabel(f"incoming requests")
        ax.tick_params(axis="x", labelrotation=labelrotation)

    def incoming_requests(self, rt_report, freq):
        """
        Aggregate the number of incoming requests over time.

        Args:
            rt_report (pd.DataFrame): The runtime report.
            freq (str): The frequency of data points (e.g., "1S").

        Returns:
            pd.Series: The aggregated number of incoming requests over time.
        """
        requests = rt_report[rt_report["message"] == "request"]
        request_ps_count = self._aggregate_entries(requests, "timestamp", freq=freq)
        return request_ps_count

    def plot_active_requests(self, ax, rt_report, freq="1S", labelrotation=0):
        """
        Plot the number of active requests over time.

        Args:
            ax (matplotlib.axes._axes.Axes): The matplotlib axes to plot on.
            rt_report (pd.DataFrame): The runtime report.
            freq (str, optional): The frequency of data points (e.g., "1S").
            labelrotation (int, optional): The rotation angle for x-axis labels (default 0).
        """
        ax.yaxis.set_major_locator(MaxNLocator(integer=True))
        active_ps_count = self.active_requests(rt_report, freq)
        active_ps_count.plot.bar(ax=ax)
        ax.set_xlabel("simulation time / s")
        ax.set_ylabel(f"active requests")
        ax.tick_params(axis="x", labelrotation=labelrotation)

    def active_requests(self, rt_report, freq):
        """
        Aggregate the number of active requests over time.

        Args:
            rt_report (pd.DataFrame): The runtime report.
            freq (str): The frequency of data points (e.g., "1S").

        Returns:
            pd.Series: The aggregated number of active requests over time.
        """
        active_sfcs = rt_report[rt_report["message"] == "sfc_active"]
        active_sfcs = active_sfcs.groupby("value").first()
        active_ps_count = self._aggregate_entries(active_sfcs, "timestamp", freq=freq)
        return active_ps_count
    
    def plot_blocking_probability_bar(self, ax, sfc_report, freq="1S", labelrotation=0):
        """
        Plot the blocking probability over time as a bar chart.

        Args:
            ax (matplotlib.axes._axes.Axes): The matplotlib axes to plot on.
            sfc_report (pd.DataFrame): The SFC report.
            freq (str, optional): The frequency of data points (e.g., "1S").
            labelrotation (int, optional): The rotation angle for x-axis labels (default 0).
        """
        req_bp_over_time = self.blocking_probability_over_time(sfc_report, freq)
        req_bp_over_time.plot.bar(ax=ax)
        ax.set_xlabel("simulation time / s")
        ax.set_ylabel("request failure rate")
        ax.tick_params(axis="x", labelrotation=labelrotation)
    
    def plot_successful_failed(self, ax, sfc_report, freq="1S", labelrotation=0):
        """
        Plot the number of successful and failed SFCs over time.

        Args:
            ax (matplotlib.axes._axes.Axes): The matplotlib axes to plot on.
            sfc_report (pd.DataFrame): The SFC report.
            freq (str, optional): The frequency of data points (e.g., "1S").
            labelrotation (int, optional): The rotation angle for x-axis labels (default 0).
        """
        ax.yaxis.set_major_locator(MaxNLocator(integer=True))
        successful_ps_count = self._aggregate_value(sfc_report, value=True, index="start", column="successful", freq=freq)
        failed_ps_count = self._aggregate_value(sfc_report, value=False, index="start", column="successful", freq=freq)
        sfc_count = pd.DataFrame(successful_ps_count)
        sfc_count["failed"] = failed_ps_count
        sfc_count.plot.bar(ax=ax)
        ax.set_xlabel("simulation time / s")
        ax.set_ylabel("Number of SFCs")
        ax.tick_params(axis="x", labelrotation=labelrotation)

    def plot_blocking_probability_over_time(self, ax, sfc_report, freq="1S", labelrotation=0, **kwargs):
        """
        Plot the blocking probability over time as a line chart.

        Args:
            ax (matplotlib.axes._axes.Axes): The matplotlib axes to plot on.
            sfc_report (pd.DataFrame): The SFC report.
            freq (str, optional): The frequency of data points (e.g., "1S").
            labelrotation (int, optional): The rotation angle for x-axis labels (default 0).
            **kwargs: Additional keyword arguments for plotting.
        """
        req_bp_over_time = self.blocking_probability_over_time(sfc_report, freq)
        self.series_line_plot(ax, req_bp_over_time, **kwargs)
        ax.set_xlabel("simulation time / s")
        ax.set_ylabel("request failure rate")
        ax.tick_params(axis="x", labelrotation=labelrotation)

    def blocking_probability_over_time(self, sfc_report, freq):
        """
        Calculate the blocking probability over time.

        Args:
            sfc_report (pd.DataFrame): The SFC report.
            freq (str): The frequency of data points (e.g., "1S").

        Returns:
            pd.Series: The blocking probability over time.
        """
        total_sfc_count = self._aggregate_entries(sfc_report, "start", freq=freq)
        failed_sfc_count = self._aggregate_value(sfc_report, value=False, index="start", column="successful", freq=freq)
        return failed_sfc_count / total_sfc_count

    def series_line_plot(self, ax, series, marker=".", **kwargs):
        """
        Create a line plot for a time series.

        Args:
            ax (matplotlib.axes._axes.Axes): The matplotlib axes to plot on.
            series (pd.Series): The time series data to plot.
            marker (str, optional): The marker style for the data points (default ".").
            **kwargs: Additional keyword arguments for plotting.
        """
        ax.plot(series.index, series, marker=marker, **kwargs)

    def set_ax_param(self, ax, ylabel, xlabel="simulation time / s", marker=".", labelrotation=0, **kwargs):
        """
        Set parameters for the matplotlib axes.

        Args:
            ax (matplotlib.axes._axes.Axes): The matplotlib axes to configure.
            ylabel (str): The label for the y-axis.
            xlabel (str, optional): The label for the x-axis (default "simulation time / s").
            marker (str, optional): The marker style for the data points (default ".").
            labelrotation (int, optional): The rotation angle for x-axis labels (default 0).
            **kwargs: Additional keyword arguments for matplotlib.
        """
        ax.set_xlabel(xlabel)
        ax.set_ylabel(ylabel)
        ax.tick_params(axis="x", labelrotation=labelrotation)
        ax.legend()

    def _aggregate_entries(self, df, column, freq="1S"):
        """
        Aggregate entries in a DataFrame based on a timestamp column and frequency.

        Args:
            df (pd.DataFrame): The DataFrame containing data with a timestamp column.
            column (str): The name of the timestamp column.
            freq (str, optional): The frequency of data points (e.g., "1S").

        Returns:
            pd.Series: The aggregated data series.
        """
        resampled_df = df.set_index(df[column])
        resampled_df.index = pd.to_datetime(resampled_df.index, unit="ms")
        resampled_df = resampled_df[column].resample(freq).count()
        resampled_df.index = resampled_df.index.view(int) // 10 ** 9
        return resampled_df

    def _aggregate_value(self, df, value, index, column, freq="1S"):
        """
        Aggregate data in a DataFrame based on a value condition, timestamp index, and frequency.

        Args:
            df (pd.DataFrame): The DataFrame containing data.
            value (bool): The condition for aggregation (True or False).
            index (str): The name of the timestamp index.
            column (str): The name of the column to aggregate.
            freq (str, optional): The frequency of data points (e.g., "1S").

        Returns:
            pd.Series: The aggregated data series.
        """
        resampled_df = df.set_index(df[index])
        resampled_df.index = pd.to_datetime(resampled_df.index, unit="ms")
        resampled_df = resampled_df[column].resample(freq).apply(lambda x: (x == value).sum())
        resampled_df.index = resampled_df.index.view(int) // 10 ** 9
        return resampled_df
