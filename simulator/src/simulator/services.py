from abc import ABC, abstractmethod

class BaseService(ABC):
    """BaseService is an abstract base class for defining service types in a simulation.

    Args:
        env (simpy.Environment): The SimPy environment representing the simulation environment.
        resource_model: The resource model used by the service.

    Attributes:
        env (simpy.Environment): The simulation environment in which the service operates.
        resource_model: The resource model instance associated with the service.

    Methods:
        new_sfc(): Generate a new SFC (Service Function Chain) deployment request.
        next_sfc_request(): Wait until the next SFC request generation.
    """

    def __init__(self, env, resource_model):
        self.env = env
        self.resource_model = resource_model(env)

    @abstractmethod
    def new_sfc(self):
        """Generate a new SFC request.

        This method generates a new SFC request based on the implemented type of service.

        Raises:
            NotImplementedError: Needs to be implemented as part of the service API
        """
        raise NotImplementedError("Please implement")

    @abstractmethod
    def next_sfc_request(self):
        """Wait until next SFC request generation.

        This method creates a simpy.Event that should yield when the next SFC generation from new_sfc() is desired.

        Raises:
            NotImplementedError: Needs to be implemented as part of the service API
        """
        raise NotImplementedError("Please implement")