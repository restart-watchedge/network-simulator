# Name
Optimized allocation of distributed ML applications as virtual functions in Multi-access Edge Computing networks

# Description
This project is the Master's thesis of Noah Ploch. A main part of it consists in the development of a use-case-specific resource allocation network simulator. The simulator aims at facilitating the evaluation of resource allocation algorithms for distributed machine learning (DML) techniques, such as Federated Learning, in the context of multi-access edge computing (MEC) networks. 

The simulator is able to simulate the resource usage of applications in a given network using discrete-event simulation (DES). For this, simulation events temporarily block resources in the network following defined latency models. The simulation events are grouped in directed acyclic graphs, thus being able to express task dependencies and model complex applications. In this simulation environment, we call the applications Service Function Chains (SFCs). The main packages used are `networkx` for network simulation and `simpy` for DES, where objects from the `simpy` package are added to a graph provided by `networkx`.

For more details on the first use-case, usage, and theoretical base of the simulator please refer to the thesis document (write an email to noah.ploch@tum.de). 

# Repository Structure
* src/simulator/ : contains the code of the simulator
* tests/ : includes the tests for the simulator
* experiments/ : contains jupyter-notebooks, simulator configuration, logging and output data for testing and experimenting efforts done with the simulator
  * YYYY_MM_DD_EXPERIMENT_NAME
    * jupyter file
    * config file
    * logs/ : Python logging. Each file named after subexperiment
* external_resources/ : includes sample configuration files, network topologies, etc. that might be outsourced at a later point
# Installation
To run the simulator, you will need to clone the repository, have a running docker environment and docker compose installed. Follow these steps:

1. Clone the repository and navigate to the source folder of the repo
2. Run `docker-compose up`. This starts a jupyter server.

# Usage
To use the simulator, you can use the link provided in the terminal output of docker-compose to visit the jupyter server. Alternatively, you can run a script within the container by executing `docker exec -it simulator /bin/bash`, navigating to your designated folder and executing the script with `python -m myscript`.

To run a simulation, the following minimum components must be implemented:
* A service generating SFCs inheriting the API of `src/simulator/services.py`.
* A resource model inheriting the API of `src/simulator/resource_models.py`.
* An algorithm placing SFC requests that inherits the API from `src/simulator/algorithm.py`.
* A JSON-file containing the network to be simulated. You can generate a network with `src/simulator/topology_gen.py`
* A JSON configuration file containing the simulation parameters. Please see an exemplary configuration file in the playground branch. 
* A simulation object inheriting the API from `src/simulator/simulation.py`. Be careful to implement your own `build_environment()` method to include your own service and resource model.

With these components implemented, it is possible to run a simulation with the following steps:
```
sim = YourSimulationObject("./your_config.json")
sim.set_algorithm(YourAlgorithm) # algorithm object must not be initialized
sim.build_environment("path_to_your_json_topo")
sim.run_simulation() # stop after an amount of requests generated that is specified in the configuration file
```

The reports at the end of the simulation can be extracted as pd.DataFrame as follows:
```
# incldues information about SFC requests, e.g. whether an SFC request was successful or not
sfc_report = sim.collector.get_sfc_report() 

# includes information wrt network resources, e.g. available capacity at a timestamp
res_report = sim.collector.get_resource_report()

# includes further runtime information, e.g. placement time for a request by an algorithm 
rt_report = sim.collector.get_runtime_report() 

# includes event-related information
task_report = sim.collector.get_event_report() 
```

Each simulation use case requires its own reporting. For further details on reporting for a specific implementation, the playground branch may help out.

## Extendability

It is also possible to extend other components of the simulator, such as the resource allocator object, the event dispatcher, the base resource object or the base event object as long as they inherit from the class provided in the simulator code.

# Contributing guidelines
Before deciding how to contribute, think if your contribution is meant for the whole simulator or only a feature. If it is a feature, please consider creating a feature branch to add your feature. Be sure the pipeline runs through. To contribute to the simulator, please follow these steps: 
1. create a new issue
2. add a new branch and merge request. Name convention is: #task_number-task-name
3. if additional testing is performed, e.g. in jupyter notebooks, provide the information in the merge request
4. only merge if the pipelines run successfully

# Support
For any issue, please contact noah.ploch@tum.de

# Authors and acknowledgment
This project was conducted under supervision of Sebastian Troia (sebastian.troia@polimi.it) and Guido Maier (guido.maier@polimi.it)

# License
The code is released under the GNU General Public License.
